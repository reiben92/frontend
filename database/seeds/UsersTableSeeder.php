<?php

use Illuminate\Database\Seeder;

use App\Models\UsersAuth as Users;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {	
    	$c = Users::where('email','noremil@ixtelecom.net')->count();

    	if($c==0){
        	$Users = new Users;
 			$Users->email = 'noremil@ixtelecom.net';
 			$Users->name = 'Sammy';
 			$Users->password = Hash::make('password');
 			$Users->save();
 		}
    }
}
