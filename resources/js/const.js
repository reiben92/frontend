const baseUrl = $('meta[name=baseUrl]').attr('content');
const apiUrl = $('meta[name=apiUrl]').attr('content');
const hostName = $('meta[name=hostName]').attr('content');

let token = `${localStorage.getItem("api_token")}`;

if(token==null || token=='null' || !token){
	window.location.href = baseUrl+'/';
}
else{
	axios.defaults.headers.common['Authorization'] = 'Bearer '+token;
}



