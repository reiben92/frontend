Vue.component('invoices-list', {
    template:`
        <div>
            <invoices-list-table :checks='checks' @update-parent-invoices='updateParentInvoices' :args='args'@show-modal-delete-invoices='showModalDeleteInvoices' :toggleGetInvoices='toggleGetInvoices' @disable-get-invoices='disableGetInvoices'></invoices-list-table>
            <invoices-delete-prompt @hide-modal-delete-invoice='hideModalDeleteInvoice' :invoice_data='invoice_data' :toggleModalDeleteInvoices='toggleModalDeleteInvoices' @get-invoices='enableGetInvoices'></invoices-delete-prompt>    
            <invoices-delete-prompt-bulk @hide-modal-delete-bulk-invoice='hideModalDeleteInvoiceBulk' :deleted_invoice_list='deleted_invoice_list' :toggleModalDeleteInvoicesBulk='toggleModalDeleteInvoicesBulk'></invoices-delete-prompt-bulk>    
        </div>
    `,
    props:['args','getInvoice','checks','invoice_list','toggleModalDeleteInvoicesBulk'],
    data(){
        return {
            toggleGetInvoices:false,
            toggleModalDeleteInvoices:false,
            invoice_data:{},
            deleted_invoice_list:[],
        }
    },
    watch:{
        'getInvoice'(a){
            if(a){
                this.enableGetInvoices();
            }
        }
    },
    methods:{
        hideModalDeleteInvoiceBulk(){
            this.$emit('show-modal-delete-invoices-bulk',false);
        },
        hideModalDeleteInvoice(){
            let data = {};
            this.invoice_data = data;
            this.toggleModalDeleteInvoices=false;
        },
        showModalDeleteInvoices(data){
            this.invoice_data = data;
            this.toggleModalDeleteInvoices=true;
        },
        disableGetInvoices(){
            this.toggleGetInvoices=false;

            this.$emit('set-get-invoice',false);
        },
        enableGetInvoices(){
            this.toggleGetInvoices=true;
        },
        updateParentInvoices(data){
            this.deleted_invoice_list = data;
            this.$emit('update-parent-invoices',data);
        }
    }
});