Vue.component('invoices', {
    template:`
        <div class="page-content container-fluid">
            <div class="row mt-0 pt-0">
                <div class="col-12">
                    <div class="card-group">
                        <div class="card" style="background-color: transparent; border-right: none;">
                            <div class="pt-0">
                                <div class="d-flex align-items-center">
                                    <div style="width: 38%;">
                                        <h3 class="mb-0" style="font-weight: 500">Invoices</h3>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="btn-group">
                                            <a v-bind:href="createInvoiceUrl">
                                                <button class="btn btn-lg btn-danger" type="button" aria-haspopup="true" aria-expanded="false">
                                                    New Invoice
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="material-card card">
                        <div class="card-body">
                            <div class="dt-buttons mb-2 mt-1">
                                <!--div class="btn-group mr-4">
                                    <button type="button" class="btn btn-outline-dark dropdown-toggle mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Batch actions
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="javascript:void(0)">Send Invoices</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Print Invoices</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Send Reminders</a>
                                        </div>
                                    </button>
                                </div-->
                            </div>
                            <div class="dt-buttons container-fluid pb-3">
                                <div class="btn-group mr-2" v-if="!invoice_checks">
                                    <button type="button" class="btn btn-outline-dark mb-3" style="margin-bottom:0!important" v-on:click="toggleInvoiceMultipleFunctions">
                                        Batch actions
                                    </button>
                                    <div class="dropdown-menu" style="display:block" v-if="toggle1">
                                        <span class="dropdown-item" style="cursor:pointer" v-on:click='enableInvoiceCheck("delete")'>Delete Bulk</span>
                                    </div>
                                </div>
                                <div class="btn-group mr-2" v-if='invoice_checks && invoice_module=="delete"' v-on:click='showModalDeleteInvoicesBulk(true)'>
                                    <button type="button" class="btn btn-success mr-3" v-if="invoice_list.length>0">
                                        <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                    </button>
                                    <button type="button" class="btn btn-danger" v-on:click='disabledInvoiceCheck()'>
                                        <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                    </button>
                                </div>
                            </div>
                            <invoices-list :toggleModalDeleteInvoicesBulk='toggleModalDeleteInvoicesBulk' @show-modal-delete-invoices-bulk='showModalDeleteInvoicesBulk' :getInvoice='getInvoice' @update-parent-invoices='updateParentInvoices' @set-get-invoice='setGetInvoice' :checks='invoice_checks' @disable-checks='disabledInvoiceCheck' :args='args'></invoices-list>
                            <div class="dt-buttons" v-if='invoice_checks && invoice_module=="delete"'>
                                <div class="btn-group mr-2 pull-right">
                                    <button type="button" class="btn btn-success mb-3 mr-3" v-if="invoice_list.length>0" v-on:click='showModalDeleteInvoicesBulk(true)'>
                                        <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                    </button>
                                    <button type="button" class="btn btn-danger mb-3" v-on:click='disabledInvoiceCheck()'>
                                        <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
        return {
            getInvoice:false,
            args:{},
            invoice_checks:false,
            toggle1:false,
            hasOpened1:false,
            invoice_module:'',
            toggleModalDeleteInvoicesBulk:false,
            invoice_list:[],
        }
    },
    mounted(){
        this.getInvoice=true;
    },
    computed:{
        createInvoiceUrl(){
            return baseUrl+'/invoice/create';
        }
    },
    methods:{
        showModalDeleteInvoicesBulk(a){
            this.toggleModalDeleteInvoicesBulk=a;
        },
        setGetInvoice(a){
            this.getInvoice = a;
        },
        enableInvoiceCheck(title){
            this.invoice_checks = true;
            this.invoice_module = title;

            if(this.invoice_module=='payment'){

            }
            else if(this.invoice_module=='delete'){

            }
        },
        updateParentInvoices(a){
            this.invoice_list = a;
        },
        disabledInvoiceCheck(){
            this.invoice_checks = false;
            this.invoice_module = '';
        },
        async clickOutside(){
            if(!this.toggle1){
                $(document).ready(()=>{
                    $('body').click(()=>{
                        if(this.hasOpened1){
                            this.toggle1=false;
                            this.hasOpened1=false;
                            $('body').unbind('click');
                        }
                    });
                });
            }
        },
        toggleInvoiceMultipleFunctions(){
            this.clickOutside().then(()=>{
                this.toggle1=true;
                this.hasOpened1 = true;
            });
        },
    }
});