Vue.component('invoices-create', {
    template:`
        <div class="row">
            <div class="col-md-12">
                <div class="card px-3">
                    <form class="form-horizontal">
                        <div class="card-body align-items-center pb-3">
                            <div class="row">
                                <h2 class="pt-2" style="font-weight: 500">New Invoice</h2>
                                <div class="ml-auto">
                                    <button type="button" class="btn btn-inverse mr-1" v-on:click='setRecurring(true)' v-if='!is_recurring'>Make Recurring</button>
                                    <button type="button" class="btn btn-info mr-1" v-on:click='createInvoice()'>Create</button>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-0">
                            <div class="card-body border">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Name</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <customers-list @re-render-invoices='renderInvoices' @set-email='setEmail' @re-render='renderManual' :invoice='invoice' :customer_objs='customer_objs' :invoice_details='invoice_details' :states='states'></customers-list>
                                                    <!--div class="input-group-append">
                                                        <button class="btn text-info" type="button" data-toggle="modal" data-target="#adduser"><i class="mdi mdi-plus-circle-outline mr-1"></i>Add new customer</button>
                                                    </div-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" placeholder="" v-model="email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 1</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="invoice_details.billing_address_line1" aria-label="With textarea" placeholder="Billing Address Line 1" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 2</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="invoice_details.billing_address_line2" aria-label="With textarea" placeholder="Billing Address Line 2" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 3</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="invoice_details.billing_address_line3"  aria-label="With textarea" placeholder="Billing Address Line 3" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing City</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="invoice_details.billing_city" placeholder="Billing City" class="form-control">
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Postcode</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="invoice_details.billing_postcode"  placeholder="Billing postcode" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Country</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='invoice_details.billing_country' data-live-search="true" id="billing_country" data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select Country</option>
                                                    <option v-for='(country,c) in countries' v-bind:value='country.country' v-bind:data-tokens='country.country'>
                                                        {{ country.country }}
                                                    </option>
                                                </select>
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing State</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='invoice_details.billing_state' data-live-search="true" id="billing_state"  data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select State</option>
                                                    <option v-for='(state,c) in states'  v-bind:value='state' v-bind:data-tokens='state'>
                                                        {{ state }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">P.O. Number</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" placeholder="P.O. Number Here" v-model="invoice_purchase_order.purchase_order_number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Quote Number</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" placeholder="Quote Number Here" v-model="invoice_quotation.quotation_number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Invoice Date</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-bind:value="invoice_details.invoice_date" readonly="readonly" style="background:white" placeholder='Select Date'>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" id="invoice-date"><i class="icon-calender"></i></span>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" v-on:click="setInvoiceDate('')"><i class="icon-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Payment Due</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-bind:value="invoice_details.payment_due" readonly="readonly" style="background:white" placeholder='Select Date'>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" id="payment-due"><i class="icon-calender"></i></span>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" v-on:click="setPaymentDueDate('')"><i class="icon-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Currency</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <currencies-list-select-create :details='invoice_summary'></currencies-list-select-create>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" v-if="hasCustomers">
                                    <div class="col-12">
                                        <div class="material-card card">
                                            <div class="card-body">
                                                <!-- Editable table -->
                                                <div id="table" class="table-editable">
                                                    <table class="table table-responsive-md" style="table-layout:fixed">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th scope="col" width="4%">
                                                                    <span class="fs-14">#</span>
                                                                </th>
                                                                <th scope="col" width="16%">
                                                                    <span class="fs-14">Product/Service</span>
                                                                </th>
                                                                <th scope="col" width="20%">
                                                                    <span class="fs-14">Description</span>
                                                                </th>
                                                                <th scope="col" width="8%">
                                                                    <span class="fs-14">Quantity</span>
                                                                </th>
                                                                <th scope="col" width="8%">
                                                                    <span class="fs-14">Rate</span>
                                                                </th>
                                                                <th scope="col" width="14%">
                                                                    <span class="fs-14">Amount</span>
                                                                </th>
                                                                <th scope="col" width="10%">
                                                                    <span class="fs-14">Discount</span>
                                                                </th>
                                                                <th scope="col" width="15%">
                                                                    <span class="fs-14">Tax</span>
                                                                </th>
                                                                <th scope="col" width="5%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody v-if='invoice_items.length>0'>
                                                            <template v-for='(item,i) in invoice_items'>
                                                                <invoices-items @set-edit-false='setEditFalse' :count='invoice_items.length' :hasLoaded='hasLoaded' :item='item' :i='i' @remove-invoice-item='removeInvoiceItem' @render-invoice-item='renderInvoiceItem'></invoices-items>
                                                            </template>
                                                        </tbody>
                                                        <tbody v-else>
                                                            <tr>
                                                                <td colspan="10" align="center">
                                                                    <span class="fs-14">There are no items.</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="row justify-content-center border-top border-bottom py-3 mx-3">
                                                        <span class="table-add mr-4">
                                                            <span class="add text-info" v-on:click='addInvoiceItems()' style="cursor:pointer">
                                                                <i class="mdi mdi-plus-circle-outline mr-1"></i>Add Line
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- Editable table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4 px-5">
                                    <div class="col-sm-12 col-lg-4 ml-auto">
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Subtotal</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ invoice_summary.sub_total }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Tax</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ invoice_summary.total_tax }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Total</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ invoice_summary.grand_total }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-8">
                                                <div class="row px-0">
                                                    <div class="col-7 px-2">
                                                        <div class="input-group">
                                                            <select class="form-control selectpicker btn-picker" v-model="invoice_summary.discount_type" id="discount_type" data-live-search="true"  data-dropup-auto="false" data-size="10">
                                                                <option value="">Select Discount</option>
                                                                <option value="Percentage">Percentage</option>
                                                                <option value="Value">Amount</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 px-0">
                                                        <input type="text" v-if="invoice_summary.discount_type" class="form-control" style="text-align:right" v-model="invoice_summary.discount_amount" v-on:change="reformatInvoiceSummary(invoice_summary.discount_amount,2,'discount_amount')" value="0.00">
                                                        <input type="text" v-else class="form-control disabled" disabled v-bind:value="invoice_summary.discount_amount" style="text-align:right" value="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right;padding-top:0.5em">{{ invoice_summary.total_discount }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label" style="font-weight: 500">Balance Due</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right; font-weight: 500">{{ invoice_summary.balance_due }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <invoices-recurring :is_recurring='is_recurring' :invoice_recurring='invoice_recurring' @set-recurring-start-date='setRecurringStartDate' @set-recurring-end-date='setRecurringEndDate'  @set-interval='setInvoiceInterval' @set-recurring='setRecurring' @reset-recurring='resetRecurring'></invoices-recurring>
                            </div>
                        </div>
                    </form>
                    <div class="card-body align-items-center mb-3">
                        <div class="row">
                            <h2 class="pt-2" style="font-weight: 500"></h2>
                            <div class="ml-auto">
                                <button type="button" class="btn btn-inverse mr-1" v-on:click='setRecurring(true)' v-if='!is_recurring'>Make Recurring</button>
                                <button type="button" class="btn btn-info mr-1" v-on:click='createInvoice()'>Create</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{ calculateAll }}
            {{ pushEmail }}
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            invoice:{
                invoice_no:'',
                po_number:'',
                quote_number:'',
            },
            terms:[],
            invoice_items:[],
            invoice_summary:{
                sub_total:'',
                total_tax:'',
                grand_total:'',
                discount_type:'',
                discount_amount:'0.00',
                balance_due:'',
                total_discount:'',
                currency_id:'',
            },
            invoice_details:{
                billing_country:'',
                billing_state:'',
                billing_postcode:'',
                billing_city:'',
                billing_address_line3:'',
                billing_address_line2:'',
                billing_address_line1:'',
                billing_email:[],
                payment_due:'',
                invoice_date:'',
                terms:'',
            },
            email:'',
            invoice_quotation:{
                quotation_number:'',
            },
            invoice_purchase_order:{
                purchase_order_number:'',
            },
            customer_objs:{
                id:'',
                customer_id:'',
                customer_addresses:[],
                customer_details:[],
                customer_contacts:[],
                hasCustomerId:0,
                setCustomerId:false,
            },
            invoice_recurring:{
                id:'',
                interval:'',
                interval_value:'',
                payment_terms:'',
                start_date:'',
                end_date:'',
            },
            countries:[],
            states:[],
            hasLoaded:false,
            hasLoadedItems:0,
            hasLoadedCounter:0,
            counter:0,
            reloadCurrencies:false,
            is_recurring:false,
        }
    },
    watch:{
        'invoice_details.billing_country'(a){
            if(!a){
                return this.invoice_details.billing_country='' ;
            }
            else{
                return this.invoice_details.billing_country=a;
            }
        },
        'invoice_details.billing_state'(a){
            if(!a){
                return this.invoice_details.billing_state='' ;
            }
            else{
                return this.invoice_details.billing_state=a;
            }
        },
    },
    mounted(){
        var url = new URL(window.location.href);
        this.customer_objs.customer_id = url.searchParams.get("customer_id");

        if(this.customer_objs.customer_id){
            this.customer_objs.hasCustomerId = 1;
        }

        $('#invoice-date').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true,
        }).on('changeDate',(datepicker)=>{
            let d =  moment(datepicker.date).format('DD/MM/YYYY');
            this.setInvoiceDate(d);
        });

        $('#payment-due').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true
        }).on('changeDate',(datepicker)=>{
            let d =  moment(datepicker.date).format('DD/MM/YYYY');
            this.setPaymentDueDate(d);
        });
        
        this.countries = Countries;
        this.refreshCountries();
        
    },
    computed:{
        hasCustomers(){
            let u = false;

            if(this.customer_objs.customer_id){
                u=true;

                if(this.invoice_items.length==0){
                    this.addInvoiceItems();
                }
            }
            else{
                for(let i=0;i<this.invoice_items.length;i++){
                    this.removeInvoiceItem(i);
                }

                u = false;
            }

            return u;
        },
        pushEmail(){
            this.invoice_details.billing_email = this.email;
        },
        calculateAll(){
            let sub_total=0;
            let total_tax=0;
            let balance_due=0;
            let grand_total =0;
            let total_discount=0;

            for(let i=0;i<this.invoice_items.length;i++){
                item_before_discount = (parseFloat(accounting.unformat(this.invoice_items[i].amount)) * parseFloat(accounting.unformat(this.invoice_items[i].quantity)) * parseFloat(accounting.unformat(this.invoice_items[i].rate)));
                item_after_discount = item_before_discount - parseFloat(accounting.unformat(this.invoice_items[i].discount)) ;

                sub_total+=item_after_discount;

                let tax_item = 0;
                let tax_rate = parseFloat(accounting.unformat(this.invoice_items[i].tax.default_tax_rate));

                if(tax_rate>0){
                    tax_item = sub_total * tax_rate/100;
                    total_tax += tax_item;
                }

                this.invoice_items[i].grand_total = tax_item + item_after_discount;
            }

            grand_total = total_tax+sub_total;

            balance_due = grand_total;

            if(this.invoice_summary.discount_type=='Value'){
                total_discount = parseFloat(accounting.unformat(this.invoice_summary.discount_amount));
                balance_due -= total_discount;
            }
            else if(this.invoice_summary.discount_type=='Percentage'){
                let discount_rate = parseFloat(accounting.unformat(this.invoice_summary.discount_amount));

                if(discount_rate>0){
                    total_discount = grand_total *  discount_rate/100;
                    balance_due -= total_discount;
                }
            }

            this.invoice_summary.sub_total = this.reformat(sub_total,2);
            this.invoice_summary.total_tax = this.reformat(total_tax,2);
            this.invoice_summary.total_discount = this.reformat(total_discount,2);
            this.invoice_summary.grand_total = this.reformat(grand_total,2);
            this.invoice_summary.balance_due = this.reformat(balance_due,2);
        },
    },
    methods:{
        setPaymentDueDate(a){
            this.invoice_details.payment_due=a;
        },
        setInvoiceDate(a){
            this.invoice_details.invoice_date=a;
        },
        setRecurringStartDate(a){
            this.invoice_recurring.start_date = a;
        },
        setRecurringEndDate(b){
            this.invoice_recurring.end_date = b;
        },
        resetRecurring(){
            this.invoice_recurring.id='';
            this.invoice_recurring.interval='';
            this.invoice_recurring.interval_value='';
            this.invoice_recurring.start_date='';
            this.invoice_recurring.end_date='';
            this.invoice_recurring.payment_terms='';
        },
        setInvoiceInterval(a){
            this.passIntervalValue(a).then(()=>{
                $('.invoice-recurring').selectpicker('refresh');
            })
        },
        async passIntervalValue(a){
             this.invoice_recurring.interval_value=a;
        },
        setRecurring(a){
            this.is_recurring=a;
        },
        renderInvoices(){
            for(let k=0;k<this.invoice_items.length;k++){
                this.invoice_items[k].re_render = 0;
            }
        },
        setEmail(y){
            this.email = y;
        },
        async removeItem(i){
            this.invoice_items.splice(i,1);
        },
        removeInvoiceItem(i){
            this.removeItem(i).then(()=>{
                for(let k=i;k<this.invoice_items.length;k++){
                    this.invoice_items[k].re_render = 0;
                }
            });
        },
        renderInvoiceItem(row){
            for(let k=0;k<this.invoice_items.length;k++){
                if(this.invoice_items[k].row==row){
                    this.invoice_items[k].re_render = 1;
                    console.log('Finish re-rendering for : ',row);
                }
            }
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        reformatItem(a,b=2,index,obj){
            let y =0;
            y = this.reformat(a,b);
            return this.invoice_items[index][obj] = y;
        },
        reformatInvoiceSummary(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);
            return this.invoice_summary[obj] = y;
        },
        refreshCountries(){
            $('#billing_country').selectpicker('refresh');
            $('#billing_country').on('changed.bs.select',()=>{
                this.states = [];
                this.invoice_details.billing_state='';

                $('#billing_country').on('hidden.bs.select',()=>{
                    $('#billing_state').selectpicker('refresh');
                });

                for(let u=0;u<this.countries.length;u++){
                    if(this.countries[u].country==this.invoice_details.billing_country){
                        this.states = this.countries[u].states;
                    }
                }

                $('#billing_state').on('shown.bs.select',()=>{
                    $(this).selectpicker('refresh');
                });
            });
        },
        addInvoiceItems(){
            let u = {'description':'',
                     'quantity':'1.00',
                     'rate':'1.00',
                     'amount':'',
                     'discount':'',
                     'tax':{
                        'tax_id':'',
                        'default_tax_rate':'',
                        'amount':'',
                     },
                     'product_id':'',
                     'grand_total':'',
                     're_render':1,
                     'row':this.counter++,
                    }
            this.invoice_items.push(u);
        },
        createInvoice(){
            $('.preloader').fadeIn();
            
            this.loading = true;
            axios.post(apiUrl+'/invoices/create',{
                    'invoice':this.invoice,
                    'invoice_items':this.invoice_items,
                    'invoice_summary':this.invoice_summary,
                    'invoice_details':this.invoice_details,
                    'invoice_purchase_order':this.invoice_purchase_order,
                    'invoice_quotation':this.invoice_quotation,
                    'invoice_customer':this.customer_objs,
                    'invoice_recurring':this.invoice_recurring,
                })
                .then((response)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        if(response.data.code==200){
                            Swal.fire({
                              type: 'success',
                              title: 'Success',
                              html: '<span>Invoice has been successfully created!</span>',
                            }).then(()=>{
                                let url = baseUrl+'/invoice/'+response.data.invoice.id;
                                window.location.href=url;
                            })
                        }
                        else{
                            Swal.fire({
                              type: 'error',
                              title: 'Error ('+response.data.code+')',
                              html: '<span>'+response.data.err_msg.join('<br/>')+'</span>',
                            });
                        }
                    });
                })
                .catch((error)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>There seems to be an error in the server.</span>',
                        });
                    });
                });
        },
        renderManual(){
            this.renderBillingCountries().then((result)=>{
                $('#billing_state').selectpicker('refresh');
            });
        },
        async renderBillingCountries(){
            $('#billing_country').selectpicker('refresh');
            for(let u=0;u<this.countries.length;u++){
                if(this.countries[u].country==this.invoice_details.billing_country){
                    this.states = this.countries[u].states;
                }
            }       
        },
        setEditFalse(){
            this.hasLoadedCounter++;

            if(this.hasLoadedCounter==this.hasLoadedItems){
                this.hasLoaded = true;
            }

            console.log(this.hasLoaded,this.hasLoadedCounter,this.hasLoadedItems);
        }
    }
});

