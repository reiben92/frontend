Vue.component('invoices-recurring', {
    template:`
        <div class="row mt-4" id="recurring" v-if="is_recurring">
            <div class="col-12">
                <div class="material-card card bg-light mb-3">
                    <div class="card-body pb-1 px-4">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="card-title">Recurring Invoice</h4>
                            </div>
                        </div>
                        <hr class="mt-2 mb-4">
                        <form>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label mt-3 mb-0">Interval</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select class="form-control invoice-recurring col-12" v-model="invoice_recurring.interval" v-on:change="resetInterval()" data-size="5" data-live-search="true">>
                                                        <option value="">Select Interval</option>
                                                        <option value="Daily">Daily</option>
                                                        <option value="Weekly">Weekly</option>
                                                        <option value="Monthly">Monthly</option>
                                                        <!--option value="Yearly">Yearly</option-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row" v-if="invoice_recurring.interval">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label mt-3 mb-0">Every</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select v-if="invoice_recurring.interval=='Daily'" class="form-control invoice-recurring" v-model="invoice_recurring.interval_value" data-size="5" data-live-search="true">>
                                                        <option value="">Select Time</option>
                                                        <option v-for="(time,i) in times" v-bind:value="time">{{ time }}</option>
                                                    </select>
                                                    <select v-else-if="invoice_recurring.interval=='Weekly'" class="form-control invoice-recurring" v-model="invoice_recurring.interval_value" data-size="5" data-live-search="true">>
                                                        <option value="">Select Day</option>
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                                    </select>
                                                    <select v-else-if="invoice_recurring.interval=='Monthly'" class="form-control invoice-recurring" v-model="invoice_recurring.interval_value" data-size="5" data-live-search="true">>
                                                        <option value="">Select Date</option>
                                                        <option v-for="(date,i) in dates" v-bind:value="date">{{ date }}</option>
                                                    </select>
                                                    <select v-else-if="invoice_recurring.interval=='Yearly'" class="form-control invoice-recurring" v-model="invoice_recurring.interval_value" data-size="5" data-live-search="true">
                                                        <option value="">Select Month</option>
                                                        <option value='January'>January</option>
                                                        <option value='February'>February</option>
                                                        <option value='March'>March</option>
                                                        <option value='April'>April</option>
                                                        <option value='May'>May</option>
                                                        <option value='June'>June</option>
                                                        <option value='July'>July</option>
                                                        <option value='August'>August</option>
                                                        <option value='September'>September</option>
                                                        <option value='October'>October</option>
                                                        <option value='November'>November</option>
                                                        <option value='December'>December</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <div class="form-group row" v-if="invoice_recurring.interval">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label mt-3 mb-0">Payment Terms</label>
                                            </div>
                                            <div class="input-group">
                                                <select class="form-control invoice-recurring" v-model="invoice_recurring.payment_terms" data-size="5" data-live-search="true">
                                                    <option value="">Select Terms</option>
                                                    <option value='1 Week'>1 Week</option>
                                                    <option value='1 Day'>1 Day</option>
                                                    <option value='15 Days'>15 Days</option>
                                                    <option value='30 Days'>30 Days</option>
                                                    <option value='1 Month'>1 Month</option>
                                                    <option value='1.5 Months'>1.5 Months</option>
                                                    <option value='2 Months'>2 Months</option>
                                                    <option value='3 Months'>3 Months</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row" v-if="invoice_recurring.interval">
                                        <div class="col-5">
                                            <div class="form-group">
                                                <label class="control-label mt-3 mb-0">Start Date</label>
                                            </div>
                                            <div class="input-group">
                                                <input type="text" class="form-control" v-bind:value='invoice_recurring.start_date' placeholder="mm/dd/yyyy" readonly style="background:transparent;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="start-date" style="cursor:pointer"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="form-group">
                                                <label class="control-label mt-3 mb-0">End Date</label>
                                            </div>
                                            <div class="input-group">
                                                <input type="text" class="form-control" v-bind:value='invoice_recurring.end_date' placeholder="mm/dd/yyyy" readonly style="background:transparent;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="end-date" style="cursor:pointer"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <hr class="mt-4 mb-4">
                        <div class="row mb-4">
                            <div class="ml-auto">
                                <button type="button" class="btn btn-danger mr-2" v-on:click="hideRecurring()"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['is_recurring','invoice_recurring'],
    data(){
        return{
            dates:[],
            times:[],
        }
    },
    watch:{
        'is_recurring'(a){
            if(a){
                this.$nextTick(() =>{
                    this.setDatePicker();
                    this.refreshSelect();
                });
            }
            else{
                this.$emit('reset-recurring');
            }
        }
    },
    mounted(){
        this.dates = [];
        this.times = [];

        for(let k=1;k<=31;k++){
            this.dates.push(k);
        }

        for(let l=0;l<=23;l++){
            let f = l;
            if(l<=9){
                f='0'+f;
            }
            this.times.push(f+':00');
        }

        this.$nextTick(() =>{
            this.setDatePicker();
            this.refreshSelect();
        });

        
    },
    methods:{
        refreshSelect(){
            $('.invoice-recurring').selectpicker('refresh');
        },
        resetInterval(){
            this.$emit('set-interval','');
            this.$nextTick(()=>{
                this.setDatePicker();
            })
            //this.$emit('reset-recurring');
        },
        hideRecurring(){
            this.$emit('set-recurring',false);
        },
        setDatePicker(){
            $('#start-date').datepicker({
                format: "dd/mm/yyyy"
            }).on('changeDate',(datepicker)=>{
                let d =  moment(datepicker.date).format('DD/MM/YYYY');
                this.$emit('set-recurring-start-date',d);
            });

            $('#end-date').datepicker({
                format: "dd/mm/yyyy"
            }).on('changeDate',(datepicker)=>{
                let d =  moment(datepicker.date).format('DD/MM/YYYY');
                this.$emit('set-recurring-end-date',d);
            });
        }
    }
});


