Vue.component('invoices-items', {
    template:`
        <tr>
            {{ hasRendered }}
            <td class="pt-4" scope="row">
                <span style="font-size:12px">
                    {{ i+1 }}.
                </span>
            </td>
            <td style="width: 180px;">
                <product-services-list-select-create :item='item' :rendered_product_service_id='rendered_product_service_id' @set-render-product-service-id='setRenderedProductServiceID'></product-services-list-select-create>
            </td>
            <td style="height:100%">
                <textarea class="form-control pl-0" v-model='item.description' placeholder="Description" style="border: none">
                </textarea>
            </td>
            <td style="width: 100px;">
                <input type="text" class="form-control pl-0" v-model='item.quantity' v-on:change='reformatItem(item.quantity,2,"quantity")' placeholder="Quantity" style="border: none;width: 90px;">
            </td>
            <td style="width: 90px;">
                <input type="text" class="form-control pl-0" v-model='item.rate' v-on:change='reformatItem(item.rate,2,"rate")' placeholder="Rate" style="border: none;width: 90px;">
            </td>
            <td style="width: 90px;">
                <input type="text" class="form-control pl-0" v-model='item.amount' v-on:change='reformatItem(item.amount,2,"amount")' placeholder="Amount" style="border: none; width: 90px">
            </td>
            <td style="width: 90px;">
                <input type="text" class="form-control pl-0" v-model='item.discount' v-on:change='reformatItem(item.discount,2,"discount")' placeholder="Discount" style="border: none; width: 90px;">
            </td>
            <td style="width: 70px;">
                <div class="ui-select">
                    <taxes-list-select-create :item='item' :rendered_tax_id='rendered_tax_id' @set-render-tax-id='setRenderedTaxID'></taxes-list-select-create>
                </div>
            </td>
            <td align="center">
                <button type="button" class="btn btn-outline-danger btn-sm" v-on:click='removeInvoiceItem(i)' style="margin-top:0.5em" v-if='count>1'>
                    <i class="ti ti-trash"></i>
                </button>
            </td>
        </tr>  
    `,
    props:['item','i','hasLoaded','count'],
    data(){
    return {
            rendered_product_service_id:1,
            rendered_tax_id:1,
            initial:0,
        }
    },
    watch:{
        'item.re_render'(a){
            if(a==0){
                this.rendered_product_service_id=0;
                this.rendered_tax_id=0;
                this.initial++;
            }
        },
    },
    computed:{
        hasRendered(){
            if(this.initial>0){
                if(this.rendered_tax_id==1 && this.rendered_product_service_id==1){
                    this.$emit('render-invoice-item',this.item.row);
                    this.initial=0;
                }
            }
        }
    },
    mounted(){
        if(!this.hasLoaded){
            this.rendered_product_service_id=0;
            this.rendered_tax_id=0;
            this.initial++;
            this.$emit('set-edit-false');
        }
    },
    methods:{
        setRenderedProductServiceID(){
            this.rendered_product_service_id=1;
        },
        setRenderedTaxID(){
            this.rendered_tax_id=1;
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        removeInvoiceItem(index){
            //console.log('removing item :', index);
            this.$emit('remove-invoice-item',index);
        },
        reformatItem(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);

            return this.item[obj]=y;
        },
    }
});
