Vue.component('invoices-delete-prompt-bulk', {
    template:`
        <div class="modal fade" id="deleteinvoicepromptbulk" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Invoice</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete these invoices?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteInvoice()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['toggleModalDeleteInvoicesBulk','deleted_invoice_list'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
        }
    },
    watch:{
        'toggleModalDeleteInvoicesBulk'(a){
            if(a){
                this.showModal();
            }
        }
    },
    computed:{
        deleted_invoice(){
            let k = [];
            for(let i=0;i<this.deleted_invoice_list.length;i++){
                k.push(this.deleted_invoice_list[i].id);
            }
            return k;
        },
    },
    methods:{
        deleteInvoice(){
            this.loading = true;
            
            axios.post(apiUrl+'/invoices/delete/bulk',{
                    'invoices':this.deleted_invoice,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Invoice has been successfully deleted!</span>',
                    }).then(()=>{
                        this.$emit('get-invoices')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(()=>{
                        this.$emit('get-invoices')
                    });
                });
           
        },
        showModal(data){
            $('#deleteinvoicepromptbulk').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
            this.invoice = data;
        },
        hideModal(y=0){
            this.invoice = {};
            this.loading=false;

            $('#deleteinvoicepromptbulk').modal('hide');
            this.$emit('hide-modal-delete-bulk-invoice');
        },
    }
});