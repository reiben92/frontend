Vue.component('sidebar', {
    template:`
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar ps-container ps-theme-default ps-active-y" data-ps-id="fe23fdc0-a235-9a19-53d9-4b4c376f1481" v-on:mouseover='showSidebar()' v-on:mouseleave='hideSidebar()'>
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li v-for="(sidebar,i) in sidebar_items" class="sidebar-item" v-bind:class="{ selected:sidebar.selected }">
                        <span class="sidebar-link" aria-expanded="false" style="border-left:0px">
                            <a v-bind:href="sidebar.link">
                                <i style="cursor:pointer" v-bind:class="sidebar.icon"></i>
                            </a>
                            <a v-if="active_sidebar" class="hide-menu" v-bind:href="sidebar.link" style="color:inherit!important;width:100%;display:inline-block">{{ sidebar.title }}</a>
                            <i v-if="active_sidebar && hasInnerSidebar(sidebar)"  v-bind:class="checkArrowClass(sidebar)" style="font-size:13px;position:absolute;right:5px;cursor:pointer" v-on:click="toggleInnerClass(sidebar)"></i>
                        </span>
                        <ul aria-expanded="false" class="first-level in" v-if='hasInnerSidebar(sidebar) && sidebar.display_inner_menu'>
                            <li class="sidebar-item" v-for='(inner,j) in sidebar.inner_menu'>
                                <span class="sidebar-link" aria-expanded="false" style="border-left:0px">
                                    <a v-bind:href="inner.link">
                                        <i v-if="!active_sidebar" class="fs-16 mdi mdi mdi-octagram" style="font-size:13px;cursor:pointer"></i>
                                    </a>
                                    <a v-if="active_sidebar" class="hide-menu" v-bind:href="inner.link" style="padding-left:3em;font-size:13px;color:inherit!important">{{ inner.title }}</a>
                                    <i v-if="active_sidebar && hasInnerSidebar(inner)"  v-bind:class="checkArrowClass(inner)" style="font-size:13px;position:absolute;right:5px;cursor:pointer;visibility:visible" v-on:click="toggleInnerClass(inner)"></i>
                                </span>

                                <ul aria-expanded="false" class="second-level in" v-if='hasInnerSidebar(inner) && inner.display_inner_menu'>
                                    <li class="sidebar-item" v-for='(second_inner,k) in inner.inner_menu'>
                                        <span class="sidebar-link" aria-expanded="false" style="border-left:0px">
                                            <a v-bind:href="second_inner.link">
                                                <i v-if="!active_sidebar" class="fs-16 mdi mdi mdi-octagram" style="font-size:13px;cursor:pointer"></i>
                                            </a>
                                            <a v-if="active_sidebar" class="hide-menu" v-bind:href="second_inner.link" style="padding-left:4em;font-size:13px;color:inherit!important">{{ second_inner.title }}</a>
                                        </span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps-scrollbar-y-rail" style="top: 0px; height: 0px; right: 3px;">
                <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;">
                </div>
            </div>
        </div>
`,
    data(){
    return {
            active_sidebar:false,
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            sidebar_items:[{  
                              "title":"Dashboard",
                              "link":baseUrl+"/",
                              "menu":"dashboard",
                              "icon":"fs-16 mdi mdi-av-timer",
                              "selected":false,
                              "inner_menu":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right"
                           },
                           {  
                              "title":"Customer",
                              "link":baseUrl+"/customer",
                              "menu":"customer",
                              "icon":"fs-16 mdi mdi-account-card-details",
                              "selected":false,
                              "inner_menu":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",

                           },
                           {  
                              "title":"Invoice",
                              "link":baseUrl+"/invoice",
                              "menu":"invoice",
                              "icon":"fs-16 mdi mdi-file-document",
                              "selected":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":[  
                                 {  
                                    "title":"Create Invoice",
                                    "link":baseUrl+"/invoice/create",
                                    "menu":"invoice,create",
                                    "selected":false,
                                    "inner_menu":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                 }
                              ],

                           },
                           {  
                              "title":"Receipt",
                              "link":baseUrl+"/receipt",
                              "menu":"receipt",
                              "icon":"fs-16 mdi mdi-receipt",
                              "selected":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":[  
                                 {  
                                    "title":"Create Receipt",
                                    "link":baseUrl+"/receipt/create",
                                    "menu":"receipt,create",
                                    "selected":false,
                                    "inner_menu":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                 }
                              ],
                           },
                             {  
                              "title":"Credit Note",
                              "link":baseUrl+"/credit_note",
                              "menu":"credit_note",
                              "icon":"fs-16 mdi mdi-wallet",
                              "selected":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":[  
                                 {  
                                    "title":"Create Credit Note",
                                    "link":baseUrl+"/credit_note/create",
                                    "menu":"credit_note,create",
                                    "selected":false,
                                    "inner_menu":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                 }
                              ],
                           },
                           /*{  
                              "title":"Debit Note",
                              "link":baseUrl+"/debit_note",
                              "menu":"debit_note",
                              "icon":"fs-14 fas fa-money-bill-alt",
                              "selected":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":[  
                                 {  
                                    "title":"Create Debit Note",
                                    "link":baseUrl+"/debit_note/create",
                                    "menu":"credit_note,create",
                                    "selected":false,
                                    "inner_menu":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                 }
                              ],
                           },*/
                           {  
                              "title":"Product & Services",
                              "link":baseUrl+"/products_services",
                              "menu":"products_services",
                              "icon":"fs-16 mdi mdi-cart",
                              "selected":false,
                              "inner_menu":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":[  
                                 {  
                                    "title":"Create Product & Services",
                                    "link":baseUrl+"/products_services/create",
                                    "menu":"products_services,create",
                                    "selected":false,
                                    "inner_menu":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                 },
                                 {  
                                    "title":"Product & Services Types",
                                    "link":baseUrl+"/products_services/types",
                                    "menu":"products_services,types",
                                    "selected":false,
                                    "display_inner_menu":false,
                                    "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                                    "inner_menu":false,/*[
                                        {
                                            "title":"Create Type",
                                            "link":baseUrl+"/products_services/types/create",
                                            "menu":"products_services,types,create",
                                            "selected":false,
                                            "inner_menu":false,
                                            "display_inner_menu":false,
                                            "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",

                                        }
                                    ]*/
                                 },
                              ],
                           },
                           {  
                              "title":"Taxes",
                              "link":baseUrl+"/taxes",
                              "menu":"taxes",
                              "icon":"fs-16 mdi mdi-percent",
                              "selected":false,
                              "inner_menu":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":false,
                           },
                           {  
                              "title":"Settings",
                              "link":baseUrl+"/settings",
                              "menu":"settings",
                              "icon":"fs-16 fas fa-cog",
                              "selected":false,
                              "inner_menu":false,
                              "display_inner_menu":false,
                              "inner_sidebar_class":"fs-16 mdi mdi-chevron-right",
                              "inner_menu":false,
                           },
                        ]
        }
    },
    mounted(){
        this.setUrl();
    },
    created(){
    },
    methods:{
        checkArrowClass(sidebar){
            return sidebar.inner_sidebar_class;
        },  
        showSidebar(){
            this.active_sidebar=true;
        },
        hideSidebar(){
            this.active_sidebar=false;
        },
        hasInnerSidebar(a){
            if(a.inner_menu){
                return true;
            }
            return false;
        },
        toggleInnerClass(sidebar){
            if(sidebar.inner_sidebar_class=='fs-16 mdi mdi-chevron-right'){
                sidebar.display_inner_menu=true;
                sidebar.inner_sidebar_class='fs-16 mdi mdi-chevron-down';
            }
            else{
                sidebar.display_inner_menu=false;
                sidebar.inner_sidebar_class='fs-16 mdi mdi-chevron-right';
            }
        },
        setUrl(){
            let urls = window.location.href;

            url = urls.split('/');

            let url_segment = [];
            let base_url_segment=[];

            url.forEach((a,index)=>{
                if(a!='http:' && a && a!=hostName){
                    url_segment.push(a)
                }
            });

            this.sidebar_items.forEach((b,index)=>{
                if(b.menu==url_segment[0]){
                    this.sidebar_items[index].selected = true;

                    if(this.sidebar_items[index].inner_menu){
                        let k = url_segment.join();

                        this.sidebar_items[index].inner_menu.forEach((c,i)=>{
                            if(this.sidebar_items[index].inner_menu[i].menu==k){
                                this.toggleInnerClass(this.sidebar_items[index]);

                                this.sidebar_items[index].inner_menu.forEach((c,index)=>{
                                });

                                //console.log(this.sidebar_items[index].inner_menu.length);
                                //if(this.sidebar_items[index].inner_menu[])
                                //this.sidebar_items[index].inner_menu[i].forEach((c,index)=>{
                                //    console.log(c,index);
                                //})
                            }
                        });
                    }
                }
            })

            $('.preloader').fadeOut(500);
        }
    }
});