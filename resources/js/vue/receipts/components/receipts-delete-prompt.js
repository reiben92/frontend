Vue.component('receipts-delete-prompt', {
    template:`
        <div class="modal fade" id="deletereceiptprompt" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Receipt</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete this receipt?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteReceipt()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            receipt:{},
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('close-modal-delete-prompt',()=>{this.hideModal();});
        VueApp.$on('show-modal-delete-prompt',(data)=>{this.showModal(data);});
    },
    methods:{
        deleteReceipt(){
            
            this.loading = true;

            axios.post(apiUrl+'/receipts/delete',{
                    'id':this.receipt.id,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Receipt has been successfully deleted!</span>',
                    }).then(function(){
                        VueApp.$emit('get-receipts')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-receipts')
                    });
                });
           
        },
        showModal(data){
            $('#deletereceiptprompt').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
            this.receipt = data;
        },
        hideModal(y=0){
            this.receipt = {};
            this.loading=false;

            $('#deletereceiptprompt').modal('hide');
        },
    }
});