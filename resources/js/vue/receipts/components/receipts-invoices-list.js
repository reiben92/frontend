Vue.component('receipts-invoices-list',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select v-bind:id='invoice_id' class="form-control btn-picker" data-live-search="true" v-model="item.invoice_id" v-on:change="passSelectObj()"  data-dropup-auto="false" data-size="10">
                  <option value="">Select Invoice</option>
                  <option v-for="(invoice,t) in invoices" v-bind:data-tokens='invoice.id' v-bind:value='invoice.id'>
                    {{ invoice.invoice_no }}
                  </option>
                </select>
            </div>
        </div>
    `,
    props:['item','customer_objs','rendered_invoice_id','invoice_data','i'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            invoice_id:'invoice-list-'+this.item.row,
            invoices:[],
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        'rendered_invoice_id'(a){
            if(a==0){
                this.getInvoices().then(()=>{
                    this.$emit('set-render-invoice-id');
                });
            }
        },
        'item.invoice_id'(a){
            if(a){
                this.$emit('set-invoices-list',this.i);
            }
            else{
                this.$emit('remove-invoices-list',this.i);
            }
        }
    },
    methods:{
        returnArgs(){
            let args = {};

            if(this.customer_objs.customer_id){
                args['customer_id'] = this.customer_objs.customer_id;
            }
            return args;
        },
        passSelectObj(){
            let d = {};
            for(let i=0;i<this.invoices.length;i++){
                if(this.invoices[i].id==this.item.invoice_id){
                    d = this.invoices[i];
                }
            }

            if(d){
                this.$emit('set-invoice-data',d);
            }
        },
        async getInvoices(){
            axios.post(apiUrl+'/invoices/list',{
                    'per_page':9999999,
                    'skip':0,
                    'args':this.returnArgs(),
                })
                .then((response)=>{
                    this.invoices = response.data.data;
                    let c=0;
                    for(let i=0;i<this.invoices.length;i++){
                        if(this.invoices[i].id==this.item.invoice_id){
                            this.passSelectObj();
                            c++;
                        }
                    }

                    if(c==0){
                        this.item.invoice_id='';
                    }
                })
                .then((response)=>{
                    $('#'+this.invoice_id).selectpicker('refresh');
                });
            
        },
        refreshList(){
            $('#'+this.invoice_id).selectpicker().on('shown.bs.select',()=>{
                this.getInvoices();
            })
        },
    }
});