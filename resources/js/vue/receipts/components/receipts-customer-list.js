Vue.component('receipts-customers-list',{
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            customers:[],
            loading:false,
        }
    },
    template:`
        <div style="width:100%">
            <div style="display:inline-block;width:100%">
                <select id='customers-list' class="form-control selectpicker btn-picker" data-live-search="true" v-model='customer_objs.customer_id' v-on:change='passSelectObj()' data-dropup-auto="false" data-size="10">
                  <option value="">Select Customer</option>
                  <option v-for="(customer,c) in customers" v-bind:data-tokens='customer.id' v-bind:value='customer.id'>
                    {{ customer.customer_code }} - {{ returnCustomerName(customer.customer_details) }} 
                  </option>
                </select>
            </div>
            <!--div style="display:inline-block;width:100%" v-if="loading">
                <div class="dropdown bootstrap-select disabled form-control">
                    <button type="button" class="btn dropdown-toggle disabled btn-light" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" data-id="customers-list" tabindex="-1" aria-disabled="true" title="Select Customer">
                        <div class="filter-option">
                            <div class="filter-option-inner">
                                <div class="filter-option-inner-inner">
                                    <img v-bind:src="loader" style="height:13px;margin-right:5px;vertical-align:middle"><span>Loading Customers</span>
                                </div>
                            </div>
                        </div>
                    </button>
                </div>
            </div-->
        </div>
    `,
    props:['receipt','customer_objs','receipt_details','loadCreditNote','loadInvoices'],
    watch:{
        'customer_objs.hasCustomerId'(a){
            if(a==1){
                this.getCustomers(a);
            }
        }
    },
    mounted(){
        this.refreshList();

        if(this.customer_objs.setCustomerId){
            this.getCustomers().then(()=>{
                this.$emit('reset-customer-id');
            });
        }
    },
    methods:{
        clearReceiptDetails(){
            this.receipt_details.billing_address_line1 = '';
            this.receipt_details.billing_address_line2 = '';
            this.receipt_details.billing_address_line3 = '';
            this.receipt_details.billing_city = '';
            this.receipt_details.billing_postcode = '';
            this.receipt_details.billing_state = '';
            this.receipt_details.billing_country = '';
            this.receipt_details.billing_email = '';

            this.$emit('set-email','');
        },
        setReceiptDetails(data){
            this.receipt_details.billing_address_line1 = data.billing_address1; 
            this.receipt_details.billing_address_line2 = data.billing_address2; 
            this.receipt_details.billing_address_line3 = data.billing_address3; 
            this.receipt_details.billing_city = data.billing_city; 
            this.receipt_details.billing_postcode = data.billing_postcode; 
            
            this.receipt_details.billing_country = data.billing_country; 
            this.receipt_details.billing_state = data.billing_state;   
        },
        passSelectObj(){
            this.passData().then((result)=>{
                this.$emit('re-render');
                this.$emit('set-load-credit-note',true);
                this.$emit('re-render-invoices');
            });
        },
        async passData(){
            this.clearReceiptDetails();

            for(let i=0;i<this.customers.length;i++){
                if(this.customers[i].id==this.customer_objs.customer_id){
                    this.customer_objs.customer_addresses = this.customers[i].customer_addresses;
                    this.customer_objs.customer_details = this.customers[i].customer_details;
                    this.customer_objs.customer_contacts = this.customers[i].customer_contacts;
                    this.customer_objs.customer_id = this.customers[i].id;
                    
                    if(this.customer_objs.customer_addresses.length>0){
                         this.setReceiptDetails(this.customer_objs.customer_addresses[0]);
                    }

                    if(this.customer_objs.customer_details.length>0){
                        this.$emit('set-email',this.customer_objs.customer_details[0].email);
                    }
                }
            }
        },
        returnCustomerName(customer_details){
            let u = [];
            for(let i=0;i<customer_details.length;i++){
                u.push(customer_details[i].customer_name);
            }

            return u.join();
        },
        async getCustomers(bool=false){
            this.loading=true;

            axios.post(apiUrl+'/customers/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=> {
                    this.customers = response.data.data;
                    this.loading = false;

                    let c=0;

                    for(let i=0;i<this.customers.length;i++){
                        if(this.customer_objs.customer_id==this.customers[i].id){
                            c++;
                        }
                    }

                    if(c==0){
                        this.customer_objs.customer_id='';
                        this.customer_objs.customer_details=[];
                        this.customer_objs.customer_contacts=[];
                        this.customer_objs.customer_addresses=[];

                        this.clearReceiptDetails();
                    }

                    if(bool==true){
                        this.passSelectObj();
                    }
                }).then((response)=>{
                     $('#customers-list').selectpicker('refresh');
                });
            
        },
        refreshList(){
            $('#customers-list').selectpicker().on('shown.bs.select',()=>{
                this.getCustomers();
            })
        },
    }
});
