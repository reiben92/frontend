Vue.component('receipt-create', {
    template:`
        <div class="row">
            <div class="col-md-12">
                <div class="card px-3">
                    <form class="form-horizontal">
                        <div class="card-body align-items-center pb-3">
                            <div class="row">
                                <h2 class="pt-2" style="font-weight: 500">New Receipt</h2>
                                <div class="ml-auto">
                                    <button type="button" class="btn btn-info mr-1" v-on:click='createReceipt()'>Create</button>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-0">
                            <div class="card-body border">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Name</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <receipts-customers-list @set-load-credit-note='setloadCreditNote' :loadCreditNote='loadCreditNote' @re-render-invoices='renderInvoices' @set-email='setEmail' @re-render='renderManual' :receipt='receipt' :customer_objs='customer_objs' :receipt_details='receipt_details' :states='states'></receipts-customers-list>
                                                    <!--div class="input-group-append">
                                                        <button class="btn text-info" type="button" data-toggle="modal" data-target="#adduser"><i class="mdi mdi-plus-circle-outline mr-1"></i>Add new customer</button>
                                                    </div-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" placeholder="" v-model="email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 1</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="receipt_details.billing_address_line1" aria-label="With textarea" placeholder="Billing Address Line 1" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 2</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="receipt_details.billing_address_line2" aria-label="With textarea" placeholder="Billing Address Line 2" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 3</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="receipt_details.billing_address_line3"  aria-label="With textarea" placeholder="Billing Address Line 3" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing City</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="receipt_details.billing_city" placeholder="Billing City" class="form-control">
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Postcode</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="receipt_details.billing_postcode"  placeholder="Billing postcode" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Country</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='receipt_details.billing_country' data-live-search="true" id="billing_country" data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select Country</option>
                                                    <option v-for='(country,c) in countries' v-bind:value='country.country' v-bind:data-tokens='country.country'>
                                                        {{ country.country }}
                                                    </option>
                                                </select>
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing State</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='receipt_details.billing_state' data-live-search="true" id="billing_state"  data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select State</option>
                                                    <option v-for='(state,c) in states'  v-bind:value='state' v-bind:data-tokens='state'>
                                                        {{ state }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Receipt Date</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-bind:value="receipt_details.receipt_date" readonly="readonly" style="background:white" placeholder='Select Date'>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style='cursor:pointer' id="receipt-date"><i class="icon-calender"></i></span>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" v-on:click="setReceiptDate('')"><i class="icon-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Receipt Description</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <textarea rows="3" class="form-control" style="background:white" v-model='receipt_details.receipt_description'></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Payment Method</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-model="receipt_details.payment_method_id" style="background:white" placeholder='Payment Method'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Deposit To</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-model="receipt_details.bank_account_id" style="background:white" placeholder='Deposit To'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" v-if="hasCustomers">
                                    <div class="col-12">
                                        <div class="material-card card">
                                            <div class="card-body">
                                                <!-- Editable table -->
                                                <h4 class="card-title pl-3">Outstanding Invoices</h4>
                                                <div id="table" class="table-editable">
                                                    <table class="table table-responsive-md" style="table-layout:fixed">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th scope="col" width="4%">
                                                                    <span style="font-size:14px">#</span>
                                                                </th>
                                                                <th scope="col" width="14%">
                                                                    <span style="font-size:14px">Invoice No </span>
                                                                </th>
                                                                <th scope="col" width="15%">
                                                                    <span style="font-size:14px">Original Amount</span>
                                                                </th>
                                                                <th scope="col" width="15%">
                                                                    <span style="font-size:14px">Balance Due</span>
                                                                </th>
                                                                <th scope="col" width="20%">
                                                                    <span style="font-size:14px">Description</span>
                                                                </th>
                                                                <th scope="col" width="15%">
                                                                    <span style="font-size:14px">Amount</span>
                                                                </th>
                                                                <th scope="col" width="12%">
                                                                    <span style="font-size:14px">Tax</span>
                                                                </th>
                                                                <th scope="col" width="4%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody v-if='receipt_items.length>0'>
                                                            <template v-for='(item,i) in receipt_items'>
                                                                <receipts-items :hasLoaded='hasLoaded' @set-parent-invoice-list='setInvoiceList' @remove-parent-invoice-list='removeInvoiceList' :customer_objs='customer_objs' :item='item' :i='i' @remove-receipt-item='removeReceiptItem' @render-receipt-item='renderReceiptItem'></receipts-items>
                                                            </template>
                                                        </tbody>
                                                        <tbody v-else>
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    <span style="font-size:14px">There are no items.</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="row justify-content-center border-top border-bottom py-3 mx-3">
                                                        <span class="table-add mr-4">
                                                            <span class="add text-info" v-on:click='addReceiptItems()' style="cursor:pointer">
                                                                <i class="mdi mdi-plus-circle-outline mr-1"></i>Add Line
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- Editable table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4 px-5" v-if="hasCustomers">
                                    <div class="col-sm-12 col-lg-4 ml-auto">
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Subtotal</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ receipt_summary.sub_total }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Tax</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ receipt_summary.total_tax }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label" style="font-weight: 500">Grand Total</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right; font-weight: 500">{{ receipt_summary.grand_total }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" v-if="hasCustomers">
                                    <div class="col-12">
                                        <div class="material-card card">
                                            <div class="card-body">
                                                <h4 class="card-title pl-3">List of Credit Notes</h4>
                                                <credit-note-list-receipt :args='args' @set-load-credit-note='setloadCreditNote' :loadCreditNote='loadCreditNote'></credit-note-list-receipt>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body align-items-center mb-3">
                        <div class="row">
                            <h2 class="pt-2" style="font-weight: 500"></h2>
                            <div class="ml-auto">
                                <button type="button" class="btn btn-info mr-1" v-on:click='createReceipt()'>Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ calculateAll }}
            {{ pushEmail }}
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            receipt:{
                receipt_no:'',
            },
            terms:[],
            receipt_items:[],
            receipt_summary:{
                sub_total:'',
                total_tax:'',
                grand_total:'',
                balance_due:'',
            },
            receipt_details:{
                billing_country:'',
                billing_state:'',
                billing_postcode:'',
                billing_city:'',
                billing_address_line3:'',
                billing_address_line2:'',
                billing_address_line1:'',
                billing_email:[],
                receipt_date:'',
                receipt_description:'',
                payment_method_id:'',
                bank_account_id:'',
            },
            email:'',
            customer_objs:{
                id:'',
                customer_id:'',
                customer_addresses:[],
                customer_details:[],
                customer_contacts:[],
                hasCustomerId:0,
            },
            countries:[],
            states:[],
            hasLoaded:false,
            hasLoadedItems:0,
            hasLoadedCounter:0,
            counter:0,
            invoice_id:'',
            args:{
                customer_id:'',
            },
            hasLoadedCustomer:false,
            loadCreditNote:false,
            invoices_list:[],
        }
    },
    watch:{
        'receipt_details.billing_country'(a){
            if(!a){
                return this.receipt_details.billing_country='' ;
            }
            else{
                return this.receipt_details.billing_country=a;
            }
        },
        'receipt_details.billing_state'(a){
            if(!a){
                return this.receipt_details.billing_state='' ;
            }
            else{
                return this.receipt_details.billing_state=a;
            }
        }
    },
    mounted(){
        var url = new URL(window.location.href);
        this.customer_objs.customer_id = url.searchParams.get("customer_id");

        if(this.customer_objs.customer_id){
            this.customer_objs.hasCustomerId=1;
        }

        $('#receipt-date').datepicker({
            format: "dd/mm/yyyy",
        }).on('changeDate',(datepicker)=>{
            let d =  moment(datepicker.date).format('DD/MM/YYYY');
            this.setReceiptDate(d);
        });

        
        this.countries = Countries;
        this.refreshCountries();

        let invoice_id = url.searchParams.get("invoice_id");
        if(invoice_id){
            let arr = invoice_id.split(',');

            for(let i=0;i<this.receipt_items.length;i++){
                this.removeReceiptItem(i);
            }

            this.invoice_id = arr;
            this.initInvoices();
        } 
    },
    computed:{
        hasCustomers(){
            let u = false;

            if(this.customer_objs.customer_id){
                u=true;

                if(this.receipt_items.length==0){
                    this.addReceiptItems();
                }
            }
            else{
                for(let i=0;i<this.receipt_items.length;i++){
                    this.removeReceiptItem(i);
                }

                u = false;
            }

            return u;
        },
        pushEmail(){
            this.receipt_details.billing_email = this.email;
        },
        calculateAll(){
            let sub_total=0;
            let total_tax=0;
            let balance_due=0;
            let grand_total =0;

            for(let i=0;i<this.receipt_items.length;i++){
                let tax_item = 0;
                let tax_rate = parseFloat(accounting.unformat(this.receipt_items[i].tax.default_tax_rate));
                let amt = parseFloat(accounting.unformat(this.receipt_items[i].amount));

                if(tax_rate>0){
                    let tot = (amt * 100)/(tax_rate+100);

                    tax_item = amt - tot;

                    sub_total+=tot;
                    total_tax += tax_item;
                }
                else{
                    sub_total+=amt;

                }

                this.receipt_items[i].grand_total = tax_item + sub_total;
            }

            grand_total = total_tax+sub_total;

            balance_due = grand_total;
            
            this.receipt_summary.sub_total = this.reformat(sub_total,2);
            this.receipt_summary.total_tax = this.reformat(total_tax,2);
            this.receipt_summary.grand_total = this.reformat(grand_total,2);
            this.receipt_summary.balance_due = this.reformat(balance_due,2);
        },
    },
    methods:{
        setReceiptDate(a){
            this.receipt_details.receipt_date=a;
        },
        setloadCreditNote(a){
            this.loadCreditNote = a;
        },
        setInvoiceList(a){
            if(!this.invoices_list.includes(a)){
                this.invoices_list.push(a);
            }
        },
        removeInvoiceList(a){
            let index = this.invoices_list.indexOf(a);
            if (index > -1) {
              this.invoices_list.splice(index, 1);
            }
        },
        initInvoices(){
            for(let i=0;i<this.invoice_id.length;i++){

                let u = {'description':'',
                     'quantity':'1.00',
                     'rate':'1.00',
                     'amount':'',
                     'tax':{
                        'tax_id':'',
                        'default_tax_rate':'',
                        'amount':'',
                     },
                     'product_id':'',
                     'row':this.counter++,
                     'grand_total':'',
                     'invoice_id':this.invoice_id[i],
                     're_render':false,
                    }
                this.receipt_items.push(u);
            }
        },
        renderInvoices(){
            for(let k=0;k<this.receipt_items.length;k++){
                this.receipt_items[k].re_render = true;
            }
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        setEmail(y){
            this.email = y;
        },
        async removeItem(i){
            this.receipt_items.splice(i,1);
        },
        removeReceiptItem(i){
            this.removeItem(i).then(()=>{
                for(let k=i;k<this.receipt_items.length;k++){
                    this.receipt_items[k].re_render = true;
                }
            });
        },
        renderReceiptItem(row){
            for(let k=0;k<this.receipt_items.length;k++){
                if(this.receipt_items[k].row==row){
                    this.receipt_items[k].re_render = false;
                    console.log('Finish re-rendering for : ',row);
                }
            }
        },
        reformatItem(a,b=2,index,obj){
            let y =0;
            y = this.reformat(a,b);
            return this.receipt_items[index][obj] = y;
        },
        reformatReceiptSummary(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);
            return this.receipt_summary[obj] = y;
        },
        refreshCountries(){
            $('#billing_country').selectpicker('refresh');


            $('#billing_country').on('changed.bs.select',()=>{
                this.states = [];
                this.receipt_details.billing_state='';

                $('#billing_country').on('hidden.bs.select',()=>{
                    $('#billing_state').selectpicker('refresh');
                });

                for(let u=0;u<this.countries.length;u++){
                    if(this.countries[u].country==this.receipt_details.billing_country){
                        this.states = this.countries[u].states;
                    }
                }

                $('#billing_state').on('shown.bs.select',()=>{
                    $(this).selectpicker('refresh');
                });
            });
        },
        addReceiptItems(){
            let u = {'description':'',
                     'quantity':'1.00',
                     'rate':'1.00',
                     'amount':'',
                     'tax':{
                        'tax_id':'',
                        'default_tax_rate':'',
                        'amount':'',
                     },
                     'product_id':'',
                     'row':this.counter++,
                     'grand_total':'',
                     'invoice_id':'',
                     're_render':false,
                    }
            this.receipt_items.push(u);
        },
        createReceipt(){
            $('.preloader').fadeIn();
            
            this.loading = true;
            axios.post(apiUrl+'/receipts/create',{
                    'receipt':this.receipt,
                    'receipt_items':this.receipt_items,
                    'receipt_summary':this.receipt_summary,
                    'receipt_details':this.receipt_details,
                    'receipt_customer':this.customer_objs,
                })
                .then((response)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        if(response.data.code==200){
                            Swal.fire({
                              type: 'success',
                              title: 'Success',
                              html: '<span>Receipt has been successfully created!</span>',
                            }).then(()=>{
                                let url = baseUrl+'/receipt/'+response.data.receipt.id;
                                window.location.href=url;
                            })
                        }
                        else{
                            Swal.fire({
                              type: 'error',
                              title: 'Error ('+response.data.code+')',
                              html: '<span>'+response.data.err_msg+'</span>',
                            });
                        }
                    });
                })
                .catch((error)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>There seems to be an error in the server.</span>',
                        });
                    });
                });
        },
        renderManual(){
            this.renderBillingCountries().then((result)=>{
                $('#billing_state').selectpicker('refresh');
            });
        },
        async renderBillingCountries(){
            $('#billing_country').selectpicker('refresh');
            for(let u=0;u<this.countries.length;u++){
                if(this.countries[u].country==this.receipt_details.billing_country){
                    this.states = this.countries[u].states;
                }
            }       
        }
    }
});

