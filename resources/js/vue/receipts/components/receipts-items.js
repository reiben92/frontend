Vue.component('receipts-items', {
    template:`
        <tr>
            {{ hasRendered }}
            <td class="pt-4" scope="row">
                <span style="font-size:12px">
                    {{ i+1 }}.
                </span>
            </td>
            <td style="width: 100px;">
                <receipts-invoices-list :i='i' @remove-invoices-list='removeInvoiceList' @set-invoices-list='setInvoiceList' @set-invoice-data='setInvoiceData' :invoice_data='invoice_data' :item='item' :rendered_invoice_id='rendered_invoice_id' :customer_objs='customer_objs' @set-render-invoice-id='setRenderedInvoiceID'></receipts-invoices-list>
            </td>
            <td style="width: 100px;">
                <input type="text" class="form-control pl-0" v-bind:value='grand_total' disabled style="border: none;width: 90px;background:white">
            </td>
            <td style="width: 100px;">
                <input type="text" class="form-control pl-0" v-bind:value='amount_due' disabled style="border: none;width: 90px;background:white">
            </td>
            <td style="height:100%">
                <textarea class="form-control pl-0" v-model='item.description' placeholder="Description" style="border: none">
                </textarea>
            </td>
            <td style="width: 90px;">
                <input type="text" class="form-control pl-0" v-model='item.amount' v-on:change='reformatItem(item.amount,2,"amount")' placeholder="Amount" style="border: none; width: 90px">
            </td>
            <td style="width: 70px;">
                <div class="ui-select">
                    <taxes-list-select-create :item='item' :rendered_tax_id='rendered_tax_id' @set-render-tax-id='setRenderedTaxID'></taxes-list-select-create>
                </div>
            </td>
            <td align="center">
                <button type="button" class="btn btn-outline-danger btn-sm" v-on:click='removeReceiptItem(i)' style="margin-top:0.5em">
                    <i class="ti ti-trash"></i>
                </button>
            </td>
        </tr>
    `,
    props:['item','i','customer_objs','hasLoaded'],
    data(){
    return {
            invoice_data:{},
            rendered_tax_id:1,
            rendered_invoice_id:1,
            initial:0,
        }
    },
    watch:{
        'item.re_render'(a){
            if(a){
                this.rendered_tax_id=false;
                this.rendered_invoice_id=false;
                this.initial++;
            }
        },
    },
    computed:{
        hasRendered(){
            if(this.initial>0){
                if(this.rendered_tax_id==true && this.rendered_invoice_id==true){
                    this.$emit('render-receipt-item',this.item.row);
                    this.initial=0;
                }
            }
        },
        amount_due(){
            let a = '0.00';

            if(this.invoice_data){
                if(this.invoice_data.invoice_summary){
                    a = this.invoice_data.invoice_summary.balance_due;
                }
            }

            return this.reformat(a,2);
        },
        grand_total(){
            let a = '0.00';

            if(this.invoice_data){
                if(this.invoice_data.invoice_summary){
                    a = this.invoice_data.invoice_summary.grand_total;
                }
            }

            return this.reformat(a,2);
        }
    },
    mounted(){
        if(!this.hasLoaded){
            this.rendered_tax_id=false;
            this.rendered_invoice_id=false;
            this.initial++;
            this.$emit('set-edit-false');
        }
    },
    methods:{
        setInvoiceList(a){
            this.$emit('set-parent-invoice-list',a);
        },
        removeInvoiceList(a){
            this.$emit('remove-parent-invoice-list',a);
        },
        setInvoiceData(a){
            this.invoice_data=a;
        },
        setRenderedTaxID(){
            this.rendered_tax_id=true;
        },
        setRenderedInvoiceID(){
            this.rendered_invoice_id=true;
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        removeReceiptItem(index){
            this.$emit('remove-receipt-item',index);
        },
        reformatItem(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);

            return this.item[obj]=y;
        },
    }
});
