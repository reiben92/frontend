Vue.component('credit-note-delete-prompt', {
    template:`
        <div class="modal fade" id="deletecreditnoteprompt" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Credit Note</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete this credit note?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteCreditNote()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['deletedModal','credit_note_data'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
        }
    },
    mounted(){
    },
    watch:{
        'deletedModal'(a){
            if(a){
                this.showModal();
            }
            else{
                this.hideModal();
            }
        }
    },
    methods:{
        deleteCreditNote(){
            this.loading = true;

            axios.post(apiUrl+'/credit_note/delete',{
                    'id':this.credit_note_data.id,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Credit Note has been successfully deleted!</span>',
                    }).then(()=>{
                        this.$emit('get-credit-note')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(()=>{
                        this.$emit('get-credit-note')
                    });
                });
           
        },
        showModal(){
            $('#deletecreditnoteprompt').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
        },
        hideModal(){
            this.loading=false;

            $('#deletecreditnoteprompt').modal('hide');
            
            this.$emit('hide-delete-credit-note');
        
        },
    }
});