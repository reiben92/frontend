Vue.component('credit-note-types-id-list',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select v-bind:id='type_id' class="form-control btn-picker credit-note-types-id" data-live-search="true" v-model="item.credit_note_type.type_id" v-on:change="passSelectObj()"  data-dropup-auto="false" data-size="10">
                  <option value="">Select {{ item.credit_note_type.type_title }}</option>
                  <option v-for="(type_id,t) in credit_note_type_id" v-bind:value="type_id.id">{{ type_id.title }}</option>
                </select>
            </div>
        </div>
    `,
    props:['item','rendered_type_id'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            type_id:'type-id-list-'+this.item.row,
            credit_note_type_id:[],
            title:'No',
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        'rendered_type_id'(a){
            if(a==0){
                this.getCreditNoteTypeId().then(()=>{
                    this.$emit('set-render-type-id');
                    console.log('re-rendering type_id: ',this.item.row);
                });
            }
        }
    },
    methods:{
        passSelectObj(){
            for(let k=0;k<this.credit_note_type_id.length;k++){
                if(this.item.credit_note_type.type_id == this.credit_note_type_id[k].id){
                    this.item.credit_note_type.type_id_title = this.credit_note_type_id[k].title;
                }
            }
        },
        async getCreditNoteTypeId(){
            if(this.item.credit_note_type.slug){
                axios.post(apiUrl+'/'+this.item.credit_note_type.slug+'/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=>{
                    let arr = [];
                    let c = 0;

                    for(let i=0;i<response.data.data.length;i++){                    
                        if(response.data.data[i].id==this.item.credit_note_type.type_id){
                            c++;
                        }

                        if(response.data.data[i]){
                            let keys = Object.keys(response.data.data[i]);
                            let title = '';

                            keys.forEach((val,index)=>{
                                if(val.includes('_no') && val.endsWith('_no')){
                                    title = response.data.data[i][val];
                                }
                            });

                            let qr = {'id':response.data.data[i].id, 'title': title};

                            arr.push(qr);
                        }
                    }

                    this.credit_note_type_id = arr;

                    if(c==0){
                        this.item.credit_note_type.type_id='';
                        this.item.credit_note_type.type_titlee='';
                    }
                })
                .then((response)=>{
                    $('#'+this.type_id).selectpicker('refresh');
                });
            }
        },
        refreshList(){
            $('#'+this.type_id).selectpicker().on('shown.bs.select',()=>{
                this.getCreditNoteTypeId();
            })
        },
    }
});
