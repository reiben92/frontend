Vue.component('credit-note-types-list',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select v-bind:id='type_id' class="form-control btn-picker credit-note-types" data-live-search="true" v-model="item.credit_note_type.type" v-on:change="passSelectObj()"  data-dropup-auto="false" data-size="10">
                  <option value="">Select Type</option>
                  <option v-for="(type,t) in credit_note_types" v-bind:data-tokens='type.id' v-bind:value='type.id'>
                    {{ type.title }}
                  </option>
                </select>
            </div>
        </div>
    `,
    props:['item','rendered_type'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            type_id:'type-list-'+this.item.row,
            credit_note_types:[],
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        'rendered_type'(a){
            if(a==0){
                this.getCreditNoteTypes().then(()=>{
                    this.$emit('set-render-type');
                    console.log('re-rendering type: ',this.item.row, ' with value :',this.item.credit_note_type.type);
                });
            }
        }
    },
    methods:{
        clearCreditNoteTypeAndDetails(){
            this.item.credit_note_type.type='';
            this.item.credit_note_type.type_id='';
            this.item.credit_note_type.type_title='';
            this.item.credit_note_type.type_id_title='';
            this.item.credit_note_type.slug='';
        },
        passSelectObj(){
            this.passSelect().then((response)=>{
                if(response==0){
                    this.clearCreditNoteTypeAndDetails();
                }
                else{
                    VueApp.$emit('get-credit-note-type-id');
                }
            });
        },
        async passSelect(){
            let c=0;

            for(let k=0;k<this.credit_note_types.length;k++){
                if(this.item.credit_note_type.type == this.credit_note_types[k].id){
                    this.item.credit_note_type.type_title = this.credit_note_types[k].title;
                    this.item.credit_note_type.slug = this.credit_note_types[k].slug;
                    c++;
                }
            }

            return c;
        },
        async getCreditNoteTypes(){
            axios.post(apiUrl+'/credit_note/types/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=>{
                    let arr = [];

                    let c=0;

                    for(let i=0;i<response.data.data.length;i++){                    
                        if(response.data.data[i].id==this.item.credit_note_type.type){
                            c++;
                        }
                        let qr = {'id':response.data.data[i].id,
                                  'title':response.data.data[i].title,
                                  'slug':response.data.data[i].slug,
                                 };
                        arr.push(qr);
                    }

                    this.credit_note_types = arr;

                    if(c==0){
                        this.clearCreditNoteTypeAndDetails();
                    }
                })
                .then((response)=>{
                    $('#'+this.type_id).selectpicker('refresh');
                });
            
        },
        refreshList(){
            $('#'+this.type_id).selectpicker().on('shown.bs.select',()=>{
                this.getCreditNoteTypes();
            })
        },
    }
});
