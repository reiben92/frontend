Vue.component('credit-note-list-receipt-items', {
    template:`
        <tr role="row" class="odd">
            <td class="sorting_1 v-t-m">
                <!--input type="checkbox"-->
                <span class="fs-14">
                    <input type="checkbox" v-model="is_checked">
                </span>
            </td>
            <td class="v-t-m">
                <span class="fs-14">
                    {{ returnCreditNoteNo(item.credit_note_no) }}
                </span>
            </td>
            <td class="text-left v-t-m">
                <span class="fs-14">
                    {{ returnCreditNoteBalance(item.credit_note_summary) }}
                </span>
            </td>
            <td class="text-left v-t-m">
                <span class="fs-14">
                    {{ returnCreditNoteBalance(item.credit_note_summary) }}
                </span>
            </td>
            <td class="text-left v-t-m">
                <input type="text" class="form-control" placeholder="" v-model="item.payment" v-on:change='reformatItem(item.payment,2,"payment")' v-if="is_checked">
                <input type="text" disabled class="form-control disabled" placeholder="" v-else>
            </td>
        </tr>
    `,
    props:['item','i','checkAll'],
    data(){
    return {
            is_checked:false,
        }
    },
    watch:{
        checkAll(a){
            if(a){
                this.is_checked=true;
            }
            else{
                this.is_checked=false;
            }
        }
    },
    mounted(){
        this.is_checked=false;
    },
    methods:{
        reformatItem(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);

            return this.item[obj]=y;
        },
        returnCreditNoteBalance(y){
            let k='N/A';
            if(y){
                if(y.balance_due){
                    k = this.reformat(y.balance_due,2);
                }
            }
            return k;
        },
        reformat(x,y){
            return accounting.formatNumber(x,y,',','.')
        },
        returnCreditNoteNo(a){
            let u =[];

            if(a){
                u.push(a)
            }

            if(u.length>0){
                return u.join();
            }
            
            return 'N/A';
        }
    }
});