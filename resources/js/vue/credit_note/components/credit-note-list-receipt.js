Vue.component('credit-note-list-receipt', {
    template:`
        <div class="table-responsive">
            <div id="alt_pagination_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    <table id="alt_pagination" class="table table-responsive-md" style="width: 100%;" role="grid" aria-describedby="alt_pagination_info">
                        <thead class="thead-light">
                            <tr role="row">
                                <th width="4%" class="sorting_asc">
                                    <!--input type="checkbox"-->
                                    <span class="fs-14"><input type="checkbox" v-model="checkAll"></span>
                                </th>
                                <th width="46%" class="sorting">
                                    <span class="fs-14">Credit Note</span>
                                </th>
                                <th width="20%" class="sorting">
                                    <span class="fs-14">Original Amount</span>
                                </th>
                                <th width="15%" class="sorting">
                                    <span class="fs-14">Open Balance</span>
                                </th>
                                <th width="14" class="sorting">
                                    <span class="fs-14">Payment</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody v-if="!loading">
                            <template v-if="credit_notes.length==0">
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <span class="fs-14">No Results.</span>
                                    </td>
                                </tr>
                            </template>
                            <template v-for="(credit_note,i) in credit_notes" v-else>
                                <credit-note-list-receipt-items :item='credit_note' :i='i' :checkAll='checkAll'></credit-note-list-receipt-items>
                            </template>
                        </tbody>
                        <tbody v-else>
                            <tr>
                                <td colspan="5" class="text-center v-t-m">
                                    <img style="height:20px;vertical-align:top;margin-right:5px" v-bind:src="loader"><span class="fs-14">Loading...</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="alt_pagination_info" role="status" aria-live="polite" v-if="!loading">
                            <span v-if="credit_notes.length>0 ">Showing {{ skip+1 }} to {{ max }} of {{ count }} entries </span>
                            <span v-else>There are no entries </span>
                        </div>
                        <div class="dataTables_info" id="alt_pagination_info" role="status" aria-live="polite" v-else>
                            
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_full_numbers pull-right" id="alt_pagination_paginate" v-if="!loading">
                            <ul class="pagination">
                                <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='firstPagination()' style="cursor:pointer">
                                    <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0">First</span>
                                </li>
                                <li class='paginate_button page-item previous disabled' v-if="pagination.length>0" id='li-prev' v-on:click='decreasePagination()' style="cursor:pointer">
                                    <span class="fs-14 page-link" href="#" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0">Previous</span>
                                </li>
                                <template v-for="(page,idx) in pagination">
                                    <li v-if="page.display" class='paginate_button page-item' v-bind:class="{active: page.active}" v-bind:li-id='page.value' v-on:click='togglePagination(page,idx)'>
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" tabindex="0">{{ page.value }}</span>
                                    </li>
                                </template>
                                <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='increasePagination()' style="cursor:pointer">
                                    <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0">Next</span>
                                </li>
                                <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='lastPagination()' style="cursor:pointer">
                                    <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0">Last</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['args','loadCreditNote'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            credit_notes:[],
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false,
            deletedModal:false,
            credit_note_data:{},
            checkAll:false,
        }
    },
    watch:{
        checkAll(a){
            if(a){
                this.checkCreditNote();
            }
            else{
                this.uncheckCreditNote();
            }
        },
        loadCreditNote(a){
            if(a){
                this.getCreditNote().then(()=>{
                });
            }
        }
    },
    mounted(){
        this.getCreditNote();
    },
    computed:{
        max(){
            let u = parseInt(this.skip)+parseInt(this.per_page);
            if(u>=this.count){
                return this.count;
            }
            else{
                return u;
            }
        }
    },
    methods:{
        reformat(x,y){
            return accounting.formatNumber(x,y,',','.')
        },
        async getCreditNote(b=0){
            if(b!=0){this.skip=0;this.current=0;this.current_page=1;}
            this.loading=true;
            axios.post(apiUrl+'/credit_note/list',{
                    'per_page':this.per_page,
                    'skip':this.skip,
                    'args':this.args,
                })
                .then((response)=>{
                    this.pagination=[];
                    this.credit_notes = response.data.data;

                    this.count = response.data.count;

                    for(g=1;g<=response.data.pagination;g++){
                        if(g==this.current+1){var isActive = true;}
                        else{var isActive = false;}

                        constant = 5;

                        if(g>constant*(this.current_page-1) && g<=constant*(this.current_page)){
                            display= true;
                        }
                        else{
                            display=false;
                        }


                        var_page = {'value':g, 'active':isActive, 'display':display};
                        this.pagination.push(var_page);
                    }

                    this.loading=false;
                    this.$emit('set-load-credit-note',false);
                })
        },
        showDeleteCreditNote(data){
            this.credit_note_data = data;
            this.deletedModal=true;
        },
        hideDeleteCreditNote(){
            this.deletedModal=false;
        },
        togglePagination(a,e){
            this.skip = (a.value*this.per_page) - this.per_page;
            
            for(x=0;x<this.pagination.length;x++){
                this.pagination[x].active = false;
            }
            this.pagination[e].active = true;
            this.current = e;
            this.getCreditNote();
        },
        decreasePagination(a){
            min = 1;    
            tag = '';

            if(this.current_page>min){
                this.current_page--;

                for(y=0;y<this.pagination.length;y++){
                    if(this.pagination[y].display==true){
                        tag = y;
                        break;
                    }
                }

                for(x=0;x<this.pagination.length;x++){
                    this.pagination[x].display = false;
                }

                tag--;

                for(z=tag;z>tag-5;z--){
                    if(this.pagination[z]){
                        this.pagination[z].display = true;
                    }
                }
            }

        },
        increasePagination(b){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            if(this.current_page<max){
                this.current_page++;
                for(y=this.pagination.length-1;y>=0;y--){
                    if(this.pagination[y].display==true){
                        tag = y;
                        break;
                    }
                }

                for(x=0;x<this.pagination.length;x++){
                    this.pagination[x].display = false;
                }

                tag++;
                for(z=tag;z<tag+5;z++){
                    if(z<this.pagination.length){
                        if(this.pagination[z]){
                            this.pagination[z].display = true;
                        }
                    }
                }
            }
        },
        lastPagination(x){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            this.current_page = max;

            for(y=this.pagination.length-1;y>=0;y--){
                this.pagination[y].display=false;
            }

            let mod = this.pagination.length%10;

            for(y=this.pagination.length-1;y>this.pagination.length-(1+mod);y--){
                this.pagination[y].display=true;
            }
        },
        firstPagination(x){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            this.current_page = 1;

            for(y=0;y<=this.pagination.length-1;y++){
                this.pagination[y].display=false;
            }

            for(y=0;y<=4;y++){
                this.pagination[y].display=true;
            }
        },
        uncheckCreditNote(){
            for(let i=0;i<this.credit_notes.length;i++){
                this.credit_notes[i].is_checked = false;
            }
        },
        checkCreditNote(){
            for(let i=0;i<this.credit_notes.length;i++){
                this.credit_notes[i].is_checked = true;
            }
        },
    }
});