Vue.component('credit-note-edit', {
    template:`
        <div class="row" v-if="loaded">
            <div class="col-md-12">
                <div class="card px-3">
                    <form class="form-horizontal">
                        <div class="card-body align-items-center pb-3">
                            <div class="row">
                                <h2 class="pt-2" style="font-weight: 500">Edit Credit Note</h2>
                                <div class="ml-auto">
                                    <button type="button" class="btn btn-info mr-1" v-on:click='createCreditNote()'>Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-0">
                            <div class="card-body border">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Name</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <customers-list @set-email='setEmail' @re-render='renderManual' @reset-customer-id='resetCustomerId' :credit_note='credit_note' :customer_objs='customer_objs' :credit_note_details='credit_note_details' :states='states'></customers-list>
                                                    <!--div class="input-group-append">
                                                        <button class="btn text-info" type="button" data-toggle="modal" data-target="#adduser"><i class="mdi mdi-plus-circle-outline mr-1"></i>Add new customer</button>
                                                    </div-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Customer Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" placeholder="" v-model="email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 1</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="credit_note_details.billing_address_line1" aria-label="With textarea" placeholder="Billing Address Line 1" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 2</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="credit_note_details.billing_address_line2" aria-label="With textarea" placeholder="Billing Address Line 2" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Address 3</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" v-model="credit_note_details.billing_address_line3"  aria-label="With textarea" placeholder="Billing Address Line 3" rows="1"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing City</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="credit_note_details.billing_city" placeholder="Billing City" class="form-control">
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Postcode</label>
                                            <div class="col-sm-3">
                                                <input type="text" id="" v-model="credit_note_details.billing_postcode"  placeholder="Billing postcode" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing Country</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='credit_note_details.billing_country' data-live-search="true" id="billing_country" data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select Country</option>
                                                    <option v-for='(country,c) in countries' v-bind:value='country.country' v-bind:data-tokens='country.country'>
                                                        {{ country.country }}
                                                    </option>
                                                </select>
                                            </div>
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Billing State</label>
                                            <div class="col-sm-3">
                                                <select class="form-control selectpicker btn-picker" v-model='credit_note_details.billing_state' data-live-search="true" id="billing_state"  data-dropup-auto="false" data-size="10">
                                                    <option value=''>Select State</option>
                                                    <option v-for='(state,c) in states'  v-bind:value='state' v-bind:data-tokens='state'>
                                                        {{ state }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Credit Note Date</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" v-bind:value="credit_note_details.credit_note_date" readonly="readonly" style="background:white" placeholder='Select Date'>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" id="credit-note-date"><i class="icon-calender"></i></span>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" style="cursor:pointer" v-on:click="setCreditNoteDate('')"><i class="icon-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <textarea rows="3" class="form-control" style="background:white" v-model='credit_note_details.credit_note_description'></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Currency</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <currencies-list-select-create @set-load-currencies='setLoadCurrencies' :currency_load='currency_load' :details='credit_note_summary'></currencies-list-select-create>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" v-if="hasCustomers">
                                    <div class="col-12">
                                        <div class="material-card card">
                                            <div class="card-body">
                                                <!-- Editable table -->
                                                <div id="table" class="table-editable">
                                                    <table class="table table-responsive-md" style="table-layout:fixed">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th scope="col" width="4%">
                                                                    <span class="fs-14">#</span>
                                                                </th>
                                                                <th scope="col" width="16%">
                                                                    <span class="fs-14">Product/Service </span>
                                                                </th>
                                                                <th scope="col" width="20%">
                                                                    <span class="fs-14">Description</span>
                                                                </th>
                                                                <th scope="col" width="8%">
                                                                    <span class="fs-14">Quantity</span>
                                                                </th>
                                                                <th scope="col" width="8%">
                                                                    <span class="fs-14">Rate</span>
                                                                </th>
                                                                <th scope="col" width="14%">
                                                                    <span class="fs-14">Amount</span>
                                                                </th>
                                                                <th scope="col" width="10%">
                                                                    <span class="fs-14">Discount</span>
                                                                </th>
                                                                <th scope="col" width="15%">
                                                                    <span class="fs-14">Tax</span>
                                                                </th>
                                                                <th scope="col" width="5%"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody v-if='credit_note_items.length>0'>
                                                            <tr v-for='(item,i) in credit_note_items'>
                                                                <td colspan="9" style="padding:0;border-top:0px">
                                                                    <credit-note-items @set-edit-false='setEditFalse' :credit_note_length='credit_note_items.length' :hasLoaded='hasLoaded' :item='item' :i='i' @remove-credit-note-item='removeCreditNoteItem' @render-credit-note-item='renderCreditNoteItem'></credit-note-items>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody v-else>
                                                            <tr>
                                                                <td colspan="9" align="center">
                                                                    <span class="fs-14">There are no items.</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    
                                                    <div class="row justify-content-center border-top border-bottom py-3 mx-3">
                                                        <span class="table-add mr-4">
                                                            <span class="add text-info" v-on:click='addCreditNoteItem()' style="cursor:pointer">
                                                                <i class="mdi mdi-plus-circle-outline mr-1"></i>Add Line
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- Editable table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4 px-5">
                                    <div class="col-sm-12 col-lg-4 ml-auto">
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Subtotal</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ credit_note_summary.sub_total }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Tax</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ credit_note_summary.total_tax }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label">Total</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right">{{ credit_note_summary.grand_total }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-8">
                                                <div class="row px-0">
                                                    <div class="col-7 px-2">
                                                        <div class="input-group">
                                                            <select class="form-control selectpicker btn-picker" v-model="credit_note_summary.discount_type" id="discount_type" data-live-search="true"  data-dropup-auto="false" data-size="10">
                                                                <option value="">Select Discount</option>
                                                                <option value="Percentage">Percentage</option>
                                                                <option value="Value">Amount</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-5 px-0">
                                                        <input type="text" v-if="credit_note_summary.discount_type" class="form-control" style="text-align:right" v-model="credit_note_summary.discount_amount" v-on:change="reformatCreditNoteSummary(credit_note_summary.discount_amount,2,'discount_amount')" value="0.00">
                                                        <input type="text" v-else class="form-control disabled" disabled v-bind:value="credit_note_summary.discount_amount" style="text-align:right" value="0.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right;padding-top:0.5em">{{ credit_note_summary.total_discount }}</span>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <label for="" class="col-sm-8 text-right control-label col-form-label" style="font-weight: 500">Balance Due</label>
                                            <div class="col-sm-4 justify-content-right">
                                                <span style="float: right; font-weight: 500">{{ credit_note_summary.balance_due }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body align-items-center mb-3">
                        <div class="row">
                            <h2 class="pt-2" style="font-weight: 500"></h2>
                            <div class="ml-auto">
                                <button type="button" class="btn btn-info mr-1" v-on:click='createCreditNote()'>Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            {{ calculateAll }}
            {{ pushEmail }}
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            credit_note:{
                credit_note_no:'',
                po_number:'',
                quote_number:'',
            },
            terms:[],
            credit_note_items:[],
            credit_note_summary:{
                sub_total:'',
                total_tax:'',
                grand_total:'',
                discount_type:'',
                discount_amount:'0.00',
                balance_due:'',
                total_discount:'',
                currency_id:'',
            },
            credit_note_details:{
                billing_country:'',
                billing_state:'',
                billing_postcode:'',
                billing_city:'',
                billing_address_line3:'',
                billing_address_line2:'',
                billing_address_line1:'',
                billing_email:[],
                credit_note_date:'',
                credit_note_description:'',
                terms:'',
            },
            email:'',
            credit_note_quotation:{
                quotation_number:'',
            },
            credit_note_purchase_order:{
                purchase_order_number:'',
            },
            customer_objs:{
                id:'',
                customer_id:'',
                customer_addresses:[],
                customer_details:[],
                customer_contacts:[],
                setCustomerId:true,
            },
            deleted_credit_note_items:[],
            countries:[],
            states:[],
            loaded:false,
            hasLoaded:false,
            hasLoadedItems:0,
            hasLoadedCounter:0,
            counter:0,
            currency_load:false,
        }
    },
    watch:{
        'credit_note_details.billing_country'(a){
            if(!a){
                return this.credit_note_details.billing_country='' ;
            }
            else{
                return this.credit_note_details.billing_country=a;
            }
        },
        'credit_note_details.billing_state'(a){
            if(!a){
                return this.credit_note_details.billing_state='' ;
            }
            else{
                return this.credit_note_details.billing_state=a;
            }
        },
    },
    mounted(){
        let url = window.location.href;

        this.credit_note.id = url[url.length-1];

        this.getCreditNote();
        this.countries = Countries;
        this.refreshCountries();
        
    },
    computed:{
        hasCustomers(){
            let u = false;

            if(this.customer_objs.customer_id){
                u=true;

                if(this.credit_note_items.length==0){
                    this.addCreditNoteItem();
                }
            }
            else{
                for(let i=0;i<this.credit_note_items.length;i++){
                    this.removeCreditNoteItem(i);
                }

                u = false;
            }

            return u;
        },
        pushEmail(){
            this.credit_note_details.billing_email = this.email;
        },
        calculateAll(){
            let sub_total=0;
            let total_tax=0;
            let balance_due=0;
            let grand_total =0;
            let total_discount=0;

            for(let i=0;i<this.credit_note_items.length;i++){
                item_before_discount = (parseFloat(accounting.unformat(this.credit_note_items[i].amount)) * parseFloat(accounting.unformat(this.credit_note_items[i].quantity)) * parseFloat(accounting.unformat(this.credit_note_items[i].rate)));
                item_after_discount = item_before_discount - parseFloat(accounting.unformat(this.credit_note_items[i].discount)) ;

                sub_total+=item_after_discount;

                let tax_item = 0;
                let tax_rate = parseFloat(accounting.unformat(this.credit_note_items[i].tax.default_tax_rate));

                if(tax_rate>0){
                    tax_item = sub_total * tax_rate/100;
                    total_tax += tax_item;
                }
                this.credit_note_items[i].grand_total = tax_item + item_after_discount;
            }

            grand_total = total_tax+sub_total;

            balance_due = grand_total;

            if(this.credit_note_summary.discount_type=='Value'){
                total_discount = parseFloat(accounting.unformat(this.credit_note_summary.discount_amount));
                balance_due -= total_discount;
            }
            else if(this.credit_note_summary.discount_type=='Percentage'){
                let discount_rate = parseFloat(accounting.unformat(this.credit_note_summary.discount_amount));

                if(discount_rate>0){
                    total_discount = grand_total *  discount_rate/100;
                    balance_due -= total_discount;
                }
            }

            this.credit_note_summary.sub_total = this.reformat(sub_total,2);
            this.credit_note_summary.total_tax = this.reformat(total_tax,2);
            this.credit_note_summary.total_discount = this.reformat(total_discount,2);
            this.credit_note_summary.grand_total = this.reformat(grand_total,2);
            this.credit_note_summary.balance_due = this.reformat(balance_due,2);
        },
    },
    methods:{
        setLoadCurrencies(a){
            this.currency_load=a;
        },
        resetCustomerId(){
            this.customer_objs.setCustomerId=false;
        },
        setEmail(y){
            this.email = y;
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        async removeItem(i){
            this.deleted_credit_note_items.push(this.credit_note_items[i]);
            this.credit_note_items.splice(i,1);
        },
        removeCreditNoteItem(i){
            this.removeItem(i).then(()=>{
                for(let k=i;k<this.credit_note_items.length;k++){
                    this.credit_note_items[k].re_render = 0;
                }
            });
        },
        renderCreditNoteItem(row){
            for(let k=0;k<this.credit_note_items.length;k++){
                if(this.credit_note_items[k].row==row){
                    this.credit_note_items[k].re_render = 1;
                    console.log('Finish re-rendering for : ',row);
                }
            }
        },
        reformatCreditNoteSummary(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);
            return this.credit_note_summary[obj] = y;
        },
        refreshCountries(){
            $('#billing_country').selectpicker('refresh');

            $('#billing_country').on('changed.bs.select',()=>{
                this.states = [];
                this.credit_note_details.billing_state='';

                $('#billing_country').on('hidden.bs.select',()=>{
                    $('#billing_state').selectpicker('refresh');
                });

                for(let u=0;u<this.countries.length;u++){
                    if(this.countries[u].country==this.credit_note_details.billing_country){
                        this.states = this.countries[u].states;
                    }
                }

                $('#billing_state').on('shown.bs.select',()=>{
                    $(this).selectpicker('refresh');
                });
            });
        },
        addCreditNoteItem(){
            let u = {'description':'',
                     'quantity':'1.00',
                     'rate':'1.00',
                     'amount':'',
                     'discount':'',
                     'tax':{
                        'tax_id':'',
                        'default_tax_rate':'',
                        'amount':'',
                     },
                     'product_id':'',
                     'row':this.counter++,
                     'grand_total':'',
                     're_render':1,
                     'id':'NEW',
                     'credit_note_type':{
                        'type':'',
                        'type_title':'',
                        'slug':'',
                        'type_id':'',
                        'type_id_title':'',
                     },
                    }
            this.credit_note_items.push(u);
        },
        createCreditNote(){
            $('.preloader').fadeIn();
            
            this.loading = true;
            axios.post(apiUrl+'/credit_note/update',{
                    'credit_note':this.credit_note,
                    'credit_note_items':this.credit_note_items,
                    'credit_note_summary':this.credit_note_summary,
                    'credit_note_details':this.credit_note_details,
                    'credit_note_customer':this.customer_objs,
                    'deleted_credit_note_items':this.deleted_credit_note_items,
                })
                .then((response)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        if(response.data.code==200){
                            Swal.fire({
                              type: 'success',
                              title: 'Success',
                              html: '<span>Credit Note has been successfully updated!</span>',
                            }).then(()=>{
                                this.getCreditNote();
                            })
                        }
                        else{
                            Swal.fire({
                              type: 'error',
                              title: 'Error ('+response.data.code+')',
                              html: '<span>'+response.data.err_msg.join("</br>")+'</span>',
                            });
                        }
                    });
                })
                .catch((error)=> {
                    this.loading = false;
                    $('.preloader').fadeOut("medium",()=>{
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>There seems to be an error in the server.</span>',
                        });
                    });
                });
        },
        getCreditNote(){
            axios.post(apiUrl+'/credit_note/get',{
                    'id':this.credit_note.id
                })
                .then((response) =>{
                    if(response.data.data){
                        this.loaded = true;
                        this.initData(response.data.data).then((response)=>{
                            this.renderManual();
                            this.initDatePicker();
                            this.initDiscountType();
                            this.currency_load=true;
                        });
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: '404',
                          html: '<span>Credit Note not found.</span>',
                        });
                    }
                })
                .catch((error)=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seems to be an error in the server.</span>',
                    });
                });
        },
        initDiscountType(){
            $('#discount_type').selectpicker('refresh');
        },
        initDatePicker(){
            $('#credit-note-date').datepicker({
                format: "dd/mm/yyyy",
                todayHighlight: true,
            }).on('changeDate',(datepicker)=>{
                let d =  moment(datepicker.date).format('DD/MM/YYYY');
                this.setCreditNoteDate(d);
            });
        },
        async initData(data){
            this.credit_note.credit_note_no = data.credit_note_no;

            let credit_note_details = {
                billing_address_line1 : data.credit_note_details.billing_address1,
                billing_address_line2 : data.credit_note_details.billing_address2,
                billing_address_line3 : data.credit_note_details.billing_address3,
                billing_city : data.credit_note_details.billing_city,
                billing_postcode : data.credit_note_details.billing_postcode,
                billing_state : data.credit_note_details.billing_state,
                billing_country : data.credit_note_details.billing_country,
                credit_note_date : data.credit_note_details.credit_note_date,
                payment_due : data.credit_note_details.payment_due,
                credit_note_description : data.credit_note_details.credit_note_description,
                id : data.credit_note_details.id,
            };

            this.email = data.credit_note_details.billing_email;
            this.credit_note_details = credit_note_details;

            if(data.credit_note_details.terms){
                this.credit_note_details.terms = data.credit_note_details.terms;
            }
            else{
                this.credit_note_details.terms = '';
            }

            let customer_id = '';
            let c_id;

            for(let j=0;j<data.credit_note_customers.length;j++){
                c_id = data.credit_note_customers[j].id
                customer_id = data.credit_note_customers[j].customer_id;
                this.customer_objs.setCustomerId = true;
            }

            this.customer_objs.id = c_id;
            this.customer_objs.customer_id = customer_id;

            let discount_type='';
            let discount_amount='0.00';
            let summary_id = '';

            if(data.credit_note_summary){
                summary_id = data.credit_note_summary.id;

                if(data.credit_note_summary.discount_type){
                    discount_type = data.credit_note_summary.discount_type;
                }
                if(data.credit_note_summary.discount_amount){
                    discount_amount = this.reformat(data.credit_note_summary.discount_amount,2);
                }

            }

            this.credit_note_summary.id = summary_id;
            this.credit_note_summary.discount_amount = discount_amount;
            this.credit_note_summary.discount_type = discount_type;
            this.credit_note_summary.currency_id = data.credit_note_summary.currency_id;

            let items =[];

            for(let u=0;u<data.credit_note_items.length;u++){
                let default_tax_rate='';
                let tax_amount='';
                let tax_id='';
                let tax_item_id='';

                if(data.credit_note_items[u].credit_note_taxes){
                    for(let k=0;k<data.credit_note_items[u].credit_note_taxes.length;k++){
                        default_tax_rate = data.credit_note_items[u].credit_note_taxes[k].tax_rate;
                        tax_id = data.credit_note_items[u].credit_note_taxes[k].tax_type_id;
                        tax_amount = data.credit_note_items[u].credit_note_taxes[k].amount;
                        tax_item_id = data.credit_note_items[u].credit_note_taxes[k].id;
                    }
                }

                let cn_type='';
                let cn_type_title='';
                let cn_type_id='';
                let cn_type_id_title='';
                let cn_slug='';

                if(data.credit_note_items[u].credit_note_types){
                    cn_type_title=data.credit_note_items[u].credit_note_types.title;
                    cn_slug=data.credit_note_items[u].credit_note_types.slug;
                }

                let obj = {'amount':this.reformat(data.credit_note_items[u].amount,2),
                           'row':this.counter++,
                           'description':data.credit_note_items[u].description,
                           'discount':this.reformat(data.credit_note_items[u].discount,2),
                           'grand_total':this.reformat(data.credit_note_items[u].grand_total,2),
                           'quantity':this.reformat(data.credit_note_items[u].quantity,2),
                           'rate':this.reformat(data.credit_note_items[u].rate,2),
                           'package_id':data.credit_note_items[u].package_id,
                           'product_id':data.credit_note_items[u].product_id,
                           'id':data.credit_note_items[u].id,
                           'credit_note_type':{
                                'type':data.credit_note_items[u].credit_note_type,
                                'type_title':cn_type_title,
                                'slug':cn_slug,
                                'type_id':data.credit_note_items[u].credit_note_type_id,
                                'type_id_title':'',
                           },
                           're_render':0,
                           'tax':{
                                'tax_id':tax_id,
                                'amount':this.reformat(tax_amount,2),
                                'default_tax_rate':default_tax_rate,
                                'id':tax_item_id,
                           }
                };
                items.push(obj);
            }

            this.credit_note_items = items;
            this.hasLoadedItems = items.length;
            this.hasLoadedCounter = 0;
        },
        renderManual(){
            this.renderBillingCountries().then((result)=>{
                $('#billing_state').selectpicker('refresh');
            });
        },
        async renderBillingCountries(){
            $('#billing_country').selectpicker('refresh');
            for(let u=0;u<this.countries.length;u++){
                if(this.countries[u].country==this.credit_note_details.billing_country){
                    this.states = this.countries[u].states;
                }
            }       
        },
        setEditFalse(){
            this.hasLoadedCounter++;

            if(this.hasLoadedCounter==this.hasLoadedItems){
                this.hasLoaded = true;
            }
        },
        setCreditNoteDate(a){
            this.credit_note_details.credit_note_date=a;
        },
    }
});