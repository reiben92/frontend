Vue.component('credit-note-items', {
    template:`
        <table class="table" style="background:transparent">
            {{ hasRendered }}
            <tbody>
                <!--tr>
                    <td width="4%" class="v-t-m">
                        <span style="font-size:12px">
                            {{ i+1 }}.
                        </span>
                    </td>
                    <td width="16%">
                        <credit-note-types-list :item='item' :rendered_type='rendered_type' @set-render-type='setRenderedType'></credit-note-types-list>
                    </td>
                    <td width="20%">
                        <credit-note-types-id-list :item='item' :rendered_type_id='rendered_type_id' @set-render-type-id='setRenderedTypeID'></credit-note-types-id-list>
                    </td>
                    <td width="8%">
                        <span>&nbsp;</span>
                    </td>
                    <td width="8%">
                        <span>&nbsp;</span>
                    </td>
                    <td width="14%">
                        <span>&nbsp;</span>
                    </td>
                    <td width="10%">
                        <span>&nbsp;</span>
                    </td>
                    <td width="15%">
                        <span>&nbsp;</span>
                    </td>
                    <td width="5%">
                        <button v-if="!item.credit_note_type.type || !item.credit_note_type.type_id" type="button" class="btn btn-outline-danger btn-sm" v-on:click='removeCreditNoteItem(i)' style="margin-top:0.5em">
                            <i class="ti ti-trash"></i>
                        </button>
                    </td>
                </tr-->
                <!--tr v-if='item.credit_note_type.type_id'-->
                <tr>
                    <td width="4%">
                        <span style="font-size:12px">
                            {{ i+1 }}.
                        </span>
                    </td>
                    <td width="16%">
                        <product-services-list-select-create :item='item' :rendered_product_service_id='rendered_product_service_id' @set-render-product-service-id='setRenderedProductServiceID'></product-services-list-select-create>
                    </td>
                    <td width="20%" style="height:100%">
                        <textarea class="form-control pl-0" v-model='item.description' placeholder="Description" style="border: none">
                        </textarea>
                    </td>
                    <td width="8%">
                        <input type="text" class="form-control pl-0" v-model='item.quantity' v-on:change='reformatItem(item.quantity,2,"quantity")' placeholder="Quantity" style="border: none;width: 90px;">
                    </td>
                    <td width="8%">
                        <input type="text" class="form-control pl-0" v-model='item.rate' v-on:change='reformatItem(item.rate,2,"rate")' placeholder="Rate" style="border: none;width: 90px;">
                    </td>
                    <td width="14%">
                        <input type="text" class="form-control pl-0" v-model='item.amount' v-on:change='reformatItem(item.amount,2,"amount")' placeholder="Amount" style="border: none; width: 90px">
                    </td>
                    <td width="10%">
                        <input type="text" class="form-control pl-0" v-model='item.discount' v-on:change='reformatItem(item.discount,2,"discount")' placeholder="Discount" style="border: none; width: 90px;">
                    </td>
                    <td width="15%">
                        <div class="ui-select">
                            <taxes-list-select-create :item='item' :rendered_tax_id='rendered_tax_id' @set-render-tax-id='setRenderedTaxID'></taxes-list-select-create>
                        </div>
                    </td>
                    <td width="5%">
                        <button type="button" class="btn btn-outline-danger btn-sm" v-on:click='removeCreditNoteItem(i)' style="margin-top:0.5em" v-if="credit_note_length>1">
                            <i class="ti ti-trash"></i>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    `,
    props:['item','i', 'hasLoaded','credit_note_length'],
    data(){
        return{
            rendered_type:1,
            rendered_type_id:1,
            rendered_product_service_id:1,
            rendered_tax_id:1,
            initial:0,
        }
    },
    watch:{
        'item.re_render'(a){
            console.log('rr',a);
            if(a==0){
                this.rendered_type=0;
                this.rendered_type_id=0;
                this.rendered_product_service_id=0;
                this.rendered_tax_id=0;
                this.initial++;
            }
        },
    },
    computed:{
        hasRendered(){
            if(this.initial>0){
                if(this.rendered_type==1 && this.rendered_type_id==1 && this.rendered_tax_id==1 && this.rendered_product_service_id==1){
                    this.$emit('render-credit-note-item',this.item.row);
                    this.initial=0;
                }
            }
        }
    },
    mounted(){
        if(!this.hasLoaded){
            this.rendered_product_service_id=0;
            this.rendered_tax_id=0;
            this.rendered_type=0;
            this.rendered_type_id=0;
            this.initial++;
            this.$emit('set-edit-false');
        }
        //console.log(this.item.re_render,this.rendered_type,this.rendered_type_id,this.rendered_product_service_id,this.rendered_tax_id)
    },
    methods:{
        setRenderedType(){
            this.rendered_type = 1;
        },
        setRenderedTypeID(){
            this.rendered_type_id = 1;
        },
        setRenderedProductServiceID(){
            this.rendered_product_service_id=1;
        },
        setRenderedTaxID(){
            this.rendered_tax_id=1;
        },
        reformat(x,y=2){
            return accounting.formatNumber(x,y,',','.')
        },
        removeCreditNoteItem(index){
            console.log('removing item :', index);
            this.$emit('remove-credit-note-item',index);
        },
        reformatItem(a,b=2,obj){
            let y =0;
            y = this.reformat(a,b);

            return this.item[obj]=y;
        },
        
    },

});
