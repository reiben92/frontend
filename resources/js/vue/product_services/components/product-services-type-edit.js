Vue.component('product-services-type-edit', {
    template:`
        <div class="modal fade" id="updateproductservicestype" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Product/Service Type</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row pt-2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Product/Service Type Title</label>
                                    <input type="text" v-model="product_services_type.title" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Product/Service Description</label>
                                    <textarea v-model="product_services_type.description" class="form-control" placeholder="" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="updateProductServicesType()">Save</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            product_services_type:{
                id:'',
                title:'',
                description:'',
            },
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('toggle-modal-edit-product-types',(id)=>{
            this.clearData(); 
            this.product_services_type.id = id;
            this.getProductServicesTypes().then(()=>{this.showModal()}) }); 
    },
    computed:{
    },
    methods:{
        showModal(data){
            $('#updateproductservicestype').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
        },
        hideModal(y=0){
            this.clearData();
            this.loading=false;

            $('#updateproductservicestype').modal('hide');
        },
        async getProductServicesTypes(){
            this.loading = true;
            axios.post(apiUrl+'/product_services/types/get',{
                    'id':this.product_services_type.id,
                }).then((response)=>{
                    if(response.data.code==200){
                        this.product_services_type = response.data.data;
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: response.data.code,
                          html: '<span>'+response.data.err_msg+'</span>',
                        });
                    }
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-product-services-types')
                    });
                });
        },
        updateProductServicesType(){
            this.loading = true;
            axios.post(apiUrl+'/product_services/types/update',{
                    'product_services_type':this.product_services_type
                }).then((response)=>{
                    if(response.data.code==200){
                        this.hideModal();
                        Swal.fire({
                          type: 'success',
                          title: 'Success',
                          html: '<span>Product/service type has been successfully updated!</span>',
                        }).then(function(){
                            VueApp.$emit('get-product-services-types')
                        });
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: response.data.code,
                          html: '<span>'+response.data.err_msg+'</span>',
                        });
                    }
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-product-services-types')
                    });
                });
        },
        clearData(){
            this.clearAllValue().then((result) => {
            });
        },
        async clearAllValue(){
            this.product_services_type.id='';
            this.product_services_type.title='';
            this.product_services_type.description='';
        }
    }
});