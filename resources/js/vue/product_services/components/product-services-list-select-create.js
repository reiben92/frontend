Vue.component('product-services-list-select-create',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select v-bind:id='product_service_id' class="form-control btn-picker product-service-list" data-live-search="true" v-model="item.product_id" v-on:change="passSelectObj()"  data-dropup-auto="false" data-size="10">
                  <option value="">Select Product/Service</option>
                  <option v-for="(product_service,t) in product_services" v-bind:data-tokens='product_service.id' v-bind:value='product_service.id'>
                    {{ returnProductServiceName(product_service) }}
                  </option>
                </select>
            </div>
        </div>
    `,
    props:['item','rendered_product_service_id'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            product_service_id:'product-services-list-'+this.item.row,
            product_services:[],
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        'rendered_product_service_id'(a){
            if(a==0){
                this.getProductServices().then(()=>{
                    this.$emit('set-render-product-service-id');
                });
            }
        }
    },
    methods:{
        initProductServices(row){
            if(row==this.item.row){
                this.getProductServices();
            }
        },
        passSelectObj(){
            
        },
        returnProductServiceName(y){
            let u =[];
            if(y.product_services_details){
                for(let k=0;k<y.product_services_details.length;k++){
                    u.push(y.product_services_details[k].product_name);
                }
            }
            
            return u.join();
        },
        async getProductServices(){
            axios.post(apiUrl+'/product_services/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=>{
                    let arr = [];
                    let c=0;

                    for(let i=0;i<response.data.data.length;i++){
                        if(response.data.data[i].id==this.item.product_id){   
                            c++;
                        }

                        let qr = response.data.data[i];

                        arr.push(qr);
                    }

                    this.product_services = arr;

                    if(c==0){
                        this.item.product_id='';                    
                    }
                })
                .then((response)=>{
                    $('#'+this.product_service_id).selectpicker('refresh');
                });
            
        },
        refreshList(){            
            $('#'+this.product_service_id).selectpicker().on('shown.bs.select',()=>{
                this.getProductServices();
            })
        },
    }
});
