Vue.component('product-services-delete-prompt', {
    template:`
        <div class="modal fade" id="deleteproductservicesprompt" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Product & Service</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete this product / service ?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteProductService()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            product_services:{},
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('close-modal-delete-prompt',()=>{this.hideModal();});
        VueApp.$on('show-modal-delete-prompt',(data)=>{this.showModal(data);});
    },
    methods:{
        deleteProductService(){
            
            this.loading = true;

            axios.post(apiUrl+'/product_services/delete',{
                    'id':this.product_services.id,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Product/Service has been successfully deleted!</span>',
                    }).then(function(){
                        VueApp.$emit('get-product-services')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-product-services')
                    });
                });
           
        },
        showModal(data){
            $('#deleteproductservicesprompt').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
            this.product_services = data;
        },
        hideModal(y=0){
            this.product_services = {};
            this.loading=false;

            $('#deleteproductservicesprompt').modal('hide');
        },
    }
});