Vue.component('product-services-create', {
    props:['args'],
    template:`
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card px-3">
                        <div class="card-body align-items-center pb-3">
                            <div class="row">
                                <h2 class="pt-2" style="font-weight: 500">New Product &amp; Service</h2>
                                <div class="ml-auto">
                                    <div class="btn-group">
                                        <button class="btn btn-info" v-on:click="createProductService()" type="button" aria-haspopup="true" aria-expanded="false">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="card mb-0">
                            <div class="card-body border">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Type</label>
                                            <div class="col-sm-9">
                                                <div class="input-group pl-0">
                                                    <select class="form-control btn-picker" id="product_type_id" v-model="product_services_details.product_type_id">>
                                                        <option value="">Select Type</option>
                                                        <option v-for="(type,t) in product_services_types" v-bind:value="type.id">{{ type.title }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3 text-right control-label">
                                                <div class="input-group-append" style="padding-left:0.75em">
                                                    <button class="btn text-info btn-no-border" type="button" v-on:click="toggleQuickAddProductType()"><i class="mdi mdi-plus-circle-outline mr-1"></i>Quick Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <div class="input-group pl-0">
                                                  <input class="form-control my-0 py-1" type="text" placeholder="" v-model="product_services_details.product_name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Accounting Code</label>
                                            <div class="col-sm-8 pr-0">
                                                <div class="input-group">
                                                    <select class="form-control btn-picker" id="accounting_id" v-model="product_services_accounting.accounting_id">
                                                        <option value="">Select Account Code</option>
                                                        <option v-for="(account,a) in accounting_id" v-bind:value="account.id">{{ account.title }}</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button class="btn text-info" type="button" v-on:click="toggleQuickAddAccounting()"><i class="mdi mdi-plus-circle-outline mr-1"></i>Quick Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mx-3 mt-4">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                            <div class="col-sm-9">
                                                <div class="input-group pl-0">
                                                  <textarea class="form-control" rows="3" placeholder="Description on Sales Forms" v-model="product_services_details.product_description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="card-body align-items-center pb-4">
                                <div class="row">
                                    <h2 class="pt-2" style="font-weight: 500"></h2>
                                    <div class="ml-auto">
                                        <div class="btn-group">
                                            <button class="btn btn-info" v-on:click="createProductService()" type="button" aria-haspopup="true" aria-expanded="false">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <product-services-type-quick-add></product-services-type-quick-add>
            <product-services-accounting-quick-add></product-services-accounting-quick-add>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            product_services_types:[],
            product_services_details:{
                product_name:'',
                product_description:'',
                product_type_id:'',
            },
            product_services_accounting:{
                accounting_id:'',
            },
            accounting_id:[],
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false
        }
    },
    mounted(){
        this.initSelectPicker();
    },
    created(){
        VueApp.$on('get-product-services-types',()=>{this.getProductServicesTypes() });
        VueApp.$on('get-accounting-code',()=>{this.getAccountingDetails() });
    },
    computed:{
    },
    methods:{
        toggleQuickAddProductType(){
            VueApp.$emit('toggle-modal-add-product-types');
        },
        toggleQuickAddAccounting(){
            VueApp.$emit('toggle-modal-add-accounting');
        },
        initSelectPicker(){
            $('#product_type_id,#accounting_id').selectpicker('refresh');
            
            $('#product_type_id').on('shown.bs.select',()=>{
                this.getProductServicesTypes();
            });

            $('#accounting_id').on('shown.bs.select',()=>{
                this.getAccountingDetails();
            });
        },
        async getProductServicesTypes(){
            axios.post(apiUrl+'/product_services/types/list',{
                    'skip':0,
                    'per_page':999,
                })
                .then((response) =>{
                    if(response.data.data){
                        this.loaded = true;
                        this.product_services_types = response.data.data;
                    }
                })
                .then(()=>{
                     $('#product_type_id').selectpicker('refresh');
                })
                .catch(function (error) {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seems to be an error in the server.</span>',
                    });
                });
        },
        async getAccountingDetails(){
            axios.post(apiUrl+'/accounting_codes/list',{
                    'skip':0,
                    'per_page':999,
                })
                .then((response) =>{
                    if(response.data.data){
                        this.loaded = true;
                        this.accounting_id = response.data.data;
                    }
                })
                .then(()=>{
                    $('#accounting_id').selectpicker('refresh');
                })
                .catch(function (error) {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seems to be an error in the server while fetching list of accounting codes.</span>',
                    });
                });
        },
        createProductService(){
            axios.post(apiUrl+'/product_services/create',{
                    'product_services_details':this.product_services_details,
                    'product_services_accounting':this.product_services_accounting,
                })
                .then((response) =>{
                    if(response.data.data){
                        Swal.fire({
                          type: 'success',
                          title: 'Success',
                          html: '<span>Successfully created a new product / service.</span>',
                        });
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>Fail to create new product / service.</span>',
                        });
                    }
                })
                .catch(function (error) {
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error in the server.</span>',
                    });
                });
        }
    }
});