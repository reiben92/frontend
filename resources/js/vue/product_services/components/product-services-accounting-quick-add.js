Vue.component('product-services-accounting-quick-add', {
    template:`
        <div class="modal fade" id="addaccounting" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add New Accounting Code</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row pt-2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Accounting Code</label>
                                    <input type="text" v-model="accounting.title" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="addAccounting()">Add</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            accounting:{
                title:'',
            },
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('toggle-modal-add-accounting',()=>{this.clearData(); this.showModal() }); 
    },
    computed:{
    },
    methods:{
        showModal(data){
            $('#addaccounting').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
        },
        hideModal(y=0){
            this.clearData();
            this.loading=false;

            $('#addaccounting').modal('hide');
        },
        addAccounting(){
            
            this.loading = true;

            axios.post(apiUrl+'/accounting_codes/create',{
                    'accounting':this.accounting
                }).then((response)=>{
                    if(response.data.code==200){
                        this.hideModal();

                        Swal.fire({
                          type: 'success',
                          title: 'Success',
                          html: '<span>Accounting code has been successfully created!</span>',
                        }).then(function(){
                            VueApp.$emit('get-accounting-code')
                        });
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: response.data.code,
                          html: '<span>'+response.data.err_msg+'</span>',
                        });
                    }
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-accounting-code')
                    });
                });
        },
        clearData(){
            this.clearAllValue().then((result) => {
            });
        },
        async clearAllValue(){
            this.accounting.title='';
        }
    }
});