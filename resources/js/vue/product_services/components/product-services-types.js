Vue.component('product-services-types', {
    props:['args'],
    template:`
        <div>
            <div class="table-responsive">
                <div id="alt_pagination_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_length" id="alt_pagination_length">
                                <label>
                                    <span style='display:inline-block'>Show</span>
                                    <select v-model='per_page' v-on:change='getProductServicesTypes(1)' name="alt_pagination_length" aria-controls="alt_pagination" class="form-control form-control-sm" style="width:70px;display:inline-block">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="1000">1000</option>
                                    </select>
                                    <span style='display:inline-block'>entries</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_full_numbers pull-right" id="alt_pagination_paginate" v-if="!loading">
                                <ul class="pagination">
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='firstPagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >First</span>
                                    </li>
                                    <li class='paginate_button page-item previous disabled' v-if="pagination.length>0" id='li-prev' v-on:click='decreasePagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" href="#" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Previous</span>
                                    </li>
                                    <template v-for="(page,idx) in pagination">
                                        <li v-if="page.display" class='paginate_button page-item' v-bind:class="{active: page.active}" v-bind:li-id='page.value' v-on:click='togglePagination(page,idx)'>
                                            <span class="fs-14 page-link" aria-controls="alt_pagination" tabindex="0" >{{ page.value }}</span>
                                        </li>
                                    </template>
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='increasePagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Next</span>
                                    </li>
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='lastPagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Last</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12">
                            <table id="alt_pagination" class="table no-wrap border display dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="alt_pagination_info">
                                <thead class="thead-light">
                                    <tr role="row">
                                        <th width="5%" class="sorting_asc">
                                            <!--input type="checkbox"-->
                                            #.
                                        </th>
                                        <th width="15%" class="sorting">
                                            <span class="fs-14">Name</span>
                                        </th>
                                        <th width="75%" class="sorting">
                                            <span class="fs-14">Description</span>
                                        </th>
                                        <th width="5%" class="sorting">
                                            <span class="fs-14">Action</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody v-if="!loading">
                                    <tr v-if="product_services_types.length==0">
                                        <td colspan="10" class="text-center">
                                            <span class="fs-14">No Results.</span>
                                        </td>
                                    </tr>
                                    <tr role="row" class="odd" v-for="(type,i) in product_services_types" v-else>
                                        <td class="v-t-m sorting_1">  
                                            <span class="fs-14">{{ skip+(i+1) }}</span>
                                        </td>
                                        <td class="v-t-m sorting_1">  
                                            <span class="fs-14">{{ type.title }}</span>
                                        </td>
                                        <td class="v-t-m sorting_1">  
                                            <span class="fs-14">{{ type.description }}</span>
                                        </td>
                                        <td class="v-t-m text-left">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-circle btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                <div class="dropdown-menu">
                                                    <span style="cursor:pointer" class="dropdown-item" v-on:click="toggleEditProductServicesTypes(type.id)">View/Edit</span>
                                                    <span style="cursor:pointer" class="dropdown-item" v-on:click="toggleDeleteProductServicesTypes(type)">Delete</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td colspan="10" class="text-center v-t-m">
                                            <img style="height:20px;vertical-align:top;margin-right:5px" v-bind:src="loader"><span class="fs-14">Loading...</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="alt_pagination_info" role="status" aria-live="polite" v-if="!loading">
                                <span v-if="product_services_types.length>0 ">Showing {{ skip+1 }} to {{ max }} of {{ count }} entries </span>
                                <span v-else>There are no entries </span>
                            </div>
                            <div class="dataTables_info" id="alt_pagination_info" role="status" aria-live="polite" v-else>
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_full_numbers pull-right" id="alt_pagination_paginate" v-if="!loading">
                                <ul class="pagination">
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='firstPagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >First</span>
                                    </li>
                                    <li class='paginate_button page-item previous disabled' v-if="pagination.length>0" id='li-prev' v-on:click='decreasePagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" href="#" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Previous</span>
                                    </li>
                                    <template v-for="(page,idx) in pagination">
                                        <li v-if="page.display" class='paginate_button page-item' v-bind:class="{active: page.active}" v-bind:li-id='page.value' v-on:click='togglePagination(page,idx)'>
                                            <span class="fs-14 page-link" aria-controls="alt_pagination" tabindex="0" >{{ page.value }}</span>
                                        </li>
                                    </template>
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='increasePagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Next</span>
                                    </li>
                                    <li class='paginate_button page-item next disabled' v-if="pagination.length>5" id='li-next' v-on:click='lastPagination()' style="cursor:pointer">
                                        <span class="fs-14 page-link" aria-controls="alt_pagination" data-dt-idx="0" tabindex="0" >Last</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <product-services-type-quick-add></product-services-type-quick-add>
            <product-services-type-edit></product-services-type-edit>
            <product-services-type-delete-prompt></product-services-type-delete-prompt>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            product_services_types:[],
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false
        }
    },
    mounted(){
        this.getProductServicesTypes();
    },
    created(){
        VueApp.$on('get-product-services-types',()=>{ this.getProductServicesTypes() });
    },
    computed:{
        max(){
            let u = parseInt(this.skip)+parseInt(this.per_page);
            if(u>=this.count){
                return this.count;
            }
            else{
                return u;
            }
        }
    },
    methods:{
        reformat(x,y){
            return accounting.formatNumber(x,y,',','.')
        },
        viewProductServices(y){
            return baseUrl+'/products_services/'+y.id;
        },
        toggleEditProductServicesTypes(id){
            VueApp.$emit('toggle-modal-edit-product-types',id);
        },
        toggleDeleteProductServicesTypes(data){
            VueApp.$emit('show-modal-delete-prompt',data);
        },
        getProductServicesTypes(b=0){
            if(b!=0){this.skip=0;this.current=0;this.current_page=1;}
            this.loading=true;
            axios.post(apiUrl+'/product_services/types/list',{
                    'per_page':this.per_page,
                    'skip':this.skip,
                    'args':this.args,
                })
                .then((response)=> {
                    this.pagination=[];
                    this.product_services_types = response.data.data;
                    this.count = response.data.count;

                    for(g=1;g<=response.data.pagination;g++){
                        if(g==this.current+1){var isActive = true;}
                        else{var isActive = false;}

                        constant = 5;

                        if(g>constant*(this.current_page-1) && g<=constant*(this.current_page)){
                            display= true;
                        }
                        else{
                            display=false;
                        }


                        var_page = {'value':g, 'active':isActive, 'display':display};
                        this.pagination.push(var_page);
                    }

                    this.loading=false;
                })
        },
        deleteProductServices(data){
            VueApp.$emit('show-modal-delete-prompt',data);
        },
        togglePagination(a,e){
            this.skip = (a.value*this.per_page) - this.per_page;
            
            for(x=0;x<this.pagination.length;x++){
                this.pagination[x].active = false;
            }
            this.pagination[e].active = true;
            this.current = e;
            this.getProductServicesTypes();
        },
        decreasePagination(a){
            min = 1;    
            tag = '';

            if(this.current_page>min){
                this.current_page--;

                for(y=0;y<this.pagination.length;y++){
                    if(this.pagination[y].display==true){
                        tag = y;
                        break;
                    }
                }

                for(x=0;x<this.pagination.length;x++){
                    this.pagination[x].display = false;
                }

                tag--;

                for(z=tag;z>tag-5;z--){
                    if(this.pagination[z]){
                        this.pagination[z].display = true;
                    }
                }
            }

        },
        increasePagination(b){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            if(this.current_page<max){
                this.current_page++;
                for(y=this.pagination.length-1;y>=0;y--){
                    if(this.pagination[y].display==true){
                        tag = y;
                        break;
                    }
                }

                for(x=0;x<this.pagination.length;x++){
                    this.pagination[x].display = false;
                }

                tag++;
                for(z=tag;z<tag+5;z++){
                    if(z<this.pagination.length){
                        if(this.pagination[z]){
                            this.pagination[z].display = true;
                        }
                    }
                }
            }
        },
        lastPagination(x){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            this.current_page = max;

            for(y=this.pagination.length-1;y>=0;y--){
                this.pagination[y].display=false;
            }

            let mod = this.pagination.length%10;

            for(y=this.pagination.length-1;y>this.pagination.length-(1+mod);y--){
                this.pagination[y].display=true;
            }
        },
        firstPagination(x){
            tag='';
            max = Math.ceil(this.pagination.length/5);

            this.current_page = 1;

            for(y=0;y<=this.pagination.length-1;y++){
                this.pagination[y].display=false;
            }

            for(y=0;y<=4;y++){
                this.pagination[y].display=true;
            }
        },
    }
});