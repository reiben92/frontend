window.VueApp = new Vue;

const app = new Vue({
    el: '#main-wrapper',
    data(){
    	return{
    		args:{},
    	}
    },
    methods:{
    	toggleAddTaxes(){
    		VueApp.$emit('toggle-modal-add-taxes');
    	},
        toggleModalQuickAdd(){
            VueApp.$emit('toggle-modal-add-product-types');
        }
    },
    created(){
        axios.post(apiUrl+'/validate').then((response)=>{
                if(response.data.code==200){

                }
                else{
                    window.location.href=baseUrl;
                }
            }).catch((e)=>{
                window.location.href=baseUrl;
            });
    },
});
