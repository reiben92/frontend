Vue.component('taxes-list-select-create',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select v-bind:id='tax_id' class="form-control btn-picker tax-list" data-live-search="true" v-model="item.tax.tax_id" v-on:change="passSelectObj()"  data-dropup-auto="false" data-size="10">
                  <option value="">Select Tax</option>
                  <option v-for="(tax,t) in taxes" v-bind:data-tokens='tax.id' v-bind:value='tax.id'>
                    {{ tax.title }} - ({{ tax.default_tax_rate }}%)
                  </option>
                </select>
            </div>
            {{ calculateTaxItem }}
        </div>
    `,
    props:['item','rendered_tax_id'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            tax_id:'taxes-list-'+this.item.row,
            taxes:[],
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        'rendered_tax_id'(a){
            if(a==0){
                this.getTaxes().then(()=>{
                    this.$emit('set-render-tax-id');
                });
            }
        }
    },
    computed:{
        calculateTaxItem(){
            let y = 0;

            if(this.item.tax.default_tax_rate>0){
                y = (this.item.tax.default_tax_rate/100) * (parseFloat(accounting.unformat(this.item.amount ))) * (parseFloat(accounting.unformat(this.item.quantity ))) * (parseFloat(accounting.unformat(this.item.rate )));
            }

            this.item.tax.amount = y;
        },
    },
    methods:{
        passSelectObj(){
            let u = 0.00;

            for(let i=0;i<this.taxes.length;i++){
                if(this.taxes[i].id==this.item.tax.tax_id){
                    u = (parseFloat(accounting.unformat(this.taxes[i].default_tax_rate )));
                }
            }

            return this.item.tax.default_tax_rate = u;
        },
        async getTaxes(){
            axios.post(apiUrl+'/taxes/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=>{
                    this.taxes = response.data.data;

                    let c=0;

                    for(let i=0;i<this.taxes.length;i++){
                        if(this.taxes[i].id==this.item.tax.tax_id){
                            c++;
                        }
                    }

                    if(c==0){
                        this.item.tax.tax_id='';
                        this.item.tax.default_tax_rate='';
                        this.item.tax.amount='';
                    }
                })
                .then((response)=>{
                    $('#'+this.tax_id).selectpicker('refresh');
                });
            
        },
        refreshList(){
            $('#'+this.tax_id).selectpicker().on('shown.bs.select',()=>{
                this.getTaxes();
            })
        },
    }
});