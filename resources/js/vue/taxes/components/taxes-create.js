Vue.component('taxes-create', {
    template:`
        <div class="modal fade bs-example-modal-lg" id="addtaxes" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">New Taxes</h4>
                        <button type="button" class="close" v-on:click="hideModal(1)" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <div class="col-12 pl-1 pr-1">
                                    <label class="control-label">Tax Name</label>
                                    <input type="text" id="" class="form-control" placeholder="" v-model="taxes.title">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="col-12 pl-1 pr-1">
                                    <label class="control-label">Default Tax Rate (%)</label>
                                    <input type="text" id="" class="form-control" placeholder="" v-model="taxes.default_tax_rate" v-on:change='reformat()'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="col-12 pl-1 pr-1">
                                    <label class="control-label">Description</label>
                                    <textarea rows="3" id="" class="form-control" placeholder="" v-model="taxes.description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal(1)">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="addTaxes()">Save</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            customers:[],
            countries:[],
            states:[],
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false,
            taxes:{
                title:'',
                description:'',
                default_tax_rate:'0.00',
            }
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('toggle-modal-add-taxes',()=>{this.clearData(); this.showModal();});
    },
    methods:{
        reformat(){
            let u = accounting.formatNumber(this.taxes.default_tax_rate,2,',','.');
            return this.taxes.default_tax_rate=u;
        },
        addTaxes(){
            $('.preloader').fadeIn();
            this.hideModal();
            
            this.loading = true;
            axios.post(apiUrl+'/taxes/create',{
                    'taxes':this.taxes,
                })
                .then((response)=> {
                    this.loading = false;
                    this.clearData();
                    $('.preloader').fadeOut("medium",function(){
                        Swal.fire({
                          type: 'success',
                          title: 'Success',
                          html: '<span>A new tax has been successfully created!</span>',
                        }).then(function(){
                            VueApp.$emit('get-taxes')
                        })
                    });
                })
                .catch((error)=> {
                    this.loading = false;
                    this.clearData();
                    $('.preloader').fadeOut("medium",function(){
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>There seems to be an error in the server.</span>',
                        });
                    });
                });
        },
        showModal(){
            $('#addtaxes').modal('show');
        },
        hideModal:function(y=0){
            $('#addtaxes').modal('hide');

            if(y==1){
                this.clearData();
            }
        },
        clearData(){
            this.clearAllValue().then((result) => {
            })
        },
        async clearAllValue(){
            this.taxes.title='';
            this.taxes.description='';
            this.taxes.default_tax_rate='0.00';
        }
    }
});