Vue.component('taxes-delete-prompt', {
    template:`
        <div class="modal fade" id="deletetaxesprompt" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Taxes</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete this tax?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteTaxes()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            taxes:{
                id:'',
            },
        }
    },
    mounted(){
    },
    created(){
        VueApp.$on('close-modal-delete-prompt',()=>{this.hideModal();});
        VueApp.$on('show-modal-delete-prompt',(data)=>{this.showModal(data);});
    },
    methods:{
        deleteTaxes(){
            
            this.loading = true;

            axios.post(apiUrl+'/taxes/delete',{
                    'id':this.taxes.id,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Tax has been successfully deleted!</span>',
                    }).then(function(){
                        VueApp.$emit('get-taxes')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(function(){
                        VueApp.$emit('get-taxes')
                    });
                });
           
        },
        showModal(data){
            $('#deletetaxesprompt').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
            this.taxes = data;
        },
        hideModal(y=0){
            this.taxes = {};
            this.loading=false;

            $('#deletetaxesprompt').modal('hide');
        },
    }
});