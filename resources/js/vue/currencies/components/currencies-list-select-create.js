Vue.component('currencies-list-select-create',{
    template:`
        <div style="width:100%"">
            <div style="display:inline-block;width:100%" >
                <select class="form-control selectpicker btn-picker" v-model='details.currency_id' data-live-search="true" v-bind:id="currency_id" data-dropup-auto="false" data-size="10">
                    <option value=''>Select Currency</option>
                    <option v-for='(currency,c) in currencies' v-bind:value='currency.id'>
                        {{ currency.code }} - {{ currency.title }} 
                    </option>
                </select>
            </div>
        </div>
    `,
    props:['details','currency_load'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            currency_id:'currencies-list',
            currencies:[],
            init:0,
        }
    },
    mounted(){
        this.refreshList();
    },
    watch:{
        currency_load(a){
            if(a){
                this.getCurrencies().then(()=>{
                    this.$emit('set-load-currencies',false);
                });
            }
        },
    },
    methods:{
        passSelectObj(){
            let u = 0.00;

        },
        async getCurrencies(){
            axios.post(apiUrl+'/currencies/list',{
                    'per_page':9999999,
                    'skip':0,
                })
                .then((response)=>{
                    this.currencies = response.data.data;

                    let c=0;

                    for(let i=0;i<this.currencies.length;i++){
                        if(this.currencies[i].id==this.details.currency_id){
                            c++;
                        }
                    }
                })
                .then((response)=>{
                    $('#'+this.currency_id).selectpicker('refresh');
                });
            
        },
        refreshList(){
            $('#'+this.currency_id).selectpicker().on('shown.bs.select',()=>{
                this.getCurrencies();
            })
        },
    }
});