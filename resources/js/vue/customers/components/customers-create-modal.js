Vue.component('customers-create-modal', {
    template:`
        <div class="modal fade bs-example-modal-lg" id="addcustomer" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">New Customer</h4>
                        <button type="button" class="close" v-on:click="hideModal(1)" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label">Type</label>
                                </div>
                                
                            </div>
                            <div class="col-md-5" style="text-align:center">
                                <div class="form-group">
                                    <input type="radio" id="" class="form-control" v-on:change="hidePanels()" placeholder="" v-model="customer_details.type" value="Company" style="display:inline-block;height:10px;width:10px;vertical-align:middle;margin-left:15px">
                                    <span>Company</span>
                                    <input type="radio" id="" class="form-control" v-on:change="hidePanels()" placeholder="" v-model="customer_details.type" value="Individual" style="display:inline-block;height:10px;width:10px;vertical-align:middle;margin-left:15px">
                                    <span>Individual</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group col-4 pr-1">
                                        <label class="control-label">Title</label>
                                        <div class="input-group">
                                            <select class="form-control selectpicker btn-picker" v-model="customer_contacts.title" data-live-search="true" id="add_title">
                                                <option value="">Select Title</option>
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Miss">Miss</option>
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="form-group col-4 pl-0 pr-1">
                                        <label class="control-label">First Name</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_contacts.first_name">
                                    </div>
                                    <div class="form-group col-4 pl-0">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_contacts.last_name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" v-if="customer_details.type=='Company'">
                                <div class="form-group">
                                    <label class="control-label">Company Name</label>
                                    <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.email">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group col-4 pr-1">
                                        <label class="control-label">Phone</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.phone_no">
                                    </div>
                                    <div class="form-group col-4 pl-0 pr-1">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.mobile_no">
                                    </div>
                                    <div class="form-group col-4 pl-0">
                                        <label class="control-label">Fax</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.fax">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label class="control-label">Website</label>
                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.website">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nickname</label>
                                    <input type="text" id="" class="form-control" placeholder="" v-model="customer_details.nickname">
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" style="cursor:pointer"><span class="nav-link" v-bind:class='{ active:tabs.address }' v-on:click="toggleTab('address')" ><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Address</span></span> </li>
                                    <li class="nav-item" style="cursor:pointer" v-if="customer_details.type=='Company'"> <span v-on:click="toggleTab('tax_info')" class="nav-link" v-bind:class='{ active:tabs.tax_info }' role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Tax Info</span></span> </li>
                                    <li class="nav-item" style="cursor:pointer"><span class="nav-link" v-bind:class='{ active:tabs.payment }' v-on:click="toggleTab('payment')" role="tab"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Payment &amp; Billing</span></span> </li>                   
                                </ul>
                                <div class="tab-content tabcontent-border p-4">
                                    <div class="tab-pane" v-bind:class='{ active:tabs.address }' role="tabpanel">
                                        <div class="row">
                                            <div class="col-md-6 px-4">
                                                <div class="form-group row mb-2">
                                                    <label class="control-label">Billing Address</label>
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street" v-model="customer_addresses.billing_address1">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street Line 2" v-model="customer_addresses.billing_address2">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street Line 3" v-model="customer_addresses.billing_address3">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <input type="text" id="" class="form-control" placeholder="City" v-model="customer_addresses.billing_city">
                                                    </div>
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <input type="text" id="" class="form-control" placeholder="Postcode" v-model="customer_addresses.billing_postcode">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <select class="form-control selectpicker btn-picker" v-model='customer_addresses.billing_country' data-live-search="true" id="billing_country" data-dropup-auto="false" data-size="10">
                                                            <option value=''>Select Country</option>
                                                            <option v-for='(country,c) in countries' v-bind:value='country.country' v-bind:data-tokens='country.country'>
                                                                {{ country.country }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <select class="form-control selectpicker btn-picker" v-model='customer_addresses.billing_state' data-live-search="true" id="billing_state"  data-dropup-auto="false" data-size="10">
                                                            <option value=''>Select State</option>
                                                            <option v-for='(state,c) in states'  v-bind:value='state' v-bind:data-tokens='state'>
                                                                {{ state }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 px-4" v-if="!customer_addresses.shipping_is_same_as_billing">
                                                <div class="form-group row mb-2">
                                                    <label class="control-label">Shipping Address</label>
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street" v-model="customer_addresses.shipping_address1">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street Line 2" v-model="customer_addresses.shipping_address2">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <input type="text" id="" class="form-control pb-0" placeholder="Street Line 3" v-model="customer_addresses.shipping_address3">
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <input type="text" id="" class="form-control" placeholder="City" v-model="customer_addresses.shipping_city">
                                                    </div>
                                                    <div class="col-md-6 px-0">
                                                        <input type="text" id="" class="form-control" placeholder="Postcode" v-model="customer_addresses.shipping_postcode">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-2">
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <select class="form-control selectpicker btn-picker" v-model='customer_addresses.shipping_country' data-live-search="true" id="shipping_country" data-dropup-auto="false" data-size="10">
                                                            <option value=''>Select Country</option>
                                                            <option v-for='(country,c) in countries' v-bind:value='country.country' v-bind:data-tokens='country.country'>
                                                                {{ country.country }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 pl-0 pr-1">
                                                        <select class="form-control selectpicker btn-picker" v-model='customer_addresses.shipping_state' data-live-search="true" id="shipping_state"  data-dropup-auto="false" data-size="10">
                                                            <option value=''>Select State</option>
                                                            <option v-for='(state,c) in states'  v-bind:value='state' v-bind:data-tokens='state'>
                                                                {{ state }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 px-4">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="checkbox2" value="check" v-model="customer_addresses.shipping_is_same_as_billing" v-on:change="check_shipping()">
                                                        <label class="custom-control-label" for="checkbox2">Same as Billing Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" v-bind:class='{ active:tabs.tax_info }' role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label class="control-label">Tax Registration Number</label>
                                                <input type="text" id="" class="form-control" v-model="customer_details.tax_registration_no">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Company Registration Number</label>
                                                <input type="text" id="" class="form-control" v-model="customer_details.roc_no">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" v-bind:class='{ active:tabs.payment }' role="tabpanel">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="form-group col-6 pr-1">
                                                        <label class="control-label">Opening Balance</label>
                                                        <input type="text" id="" class="form-control" placeholder="" v-model="customer_balances.open_balance" v-on:change='reformatBalance()'>
                                                    </div>
                                                    <div class="form-group col-6 pl-0">
                                                        <label class="control-label">as of</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" readonly id="open_balance_date" style="background:white">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Preferred Payment Method</label>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <select class="form-control selectpicker btn-picker" data-live-search="true" id="default_payment" v-model="customer_details.default_payment">
                                                            <option value="">Select Payment</option>
                                                            <option value="Cash">Cash</option>
                                                            <option value="Cheque">Cheque</option>
                                                            <option value="Credit Card">Credit Card</option>
                                                            <option value="Debit Card">Debit Card</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal(1)">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="addCompany()">Save</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['toggleCreateModal'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            customers:[],
            countries:[],
            states:[],
            per_page:100,
            skip:0,
            count:0,
            pagination:[],
            current:0,
            current_page:1,
            loading:false,
            customer_details:{
                customer_name:'',
                mobile_no:'',
                fax:'',
                email:'',
                website:'',
                phone_no:'',
                nickname:'',
                type:'Company',
                roc_no:'',
                tax_registration_no:'',
                default_currency:'',
                default_payment:'',
            },
            customer_contacts:{
                first_name:'',
                last_name:'',
                title:'',
            },
            customer_balances:{
                open_balance:'0.00',
                open_balance_date:'',
            },
            customer_addresses:{
                billing_address1:'',
                billing_address2:'',
                billing_address3:'',
                billing_city:'',
                billing_postcode:'',
                billing_state:'',
                billing_country:'',

                shipping_address1:'',
                shipping_address2:'',
                shipping_address3:'',
                shipping_city:'',
                shipping_postcode:'',
                shipping_state:'',
                shipping_country:'',
                shipping_is_same_as_billing:false,
            },
            tabs:{
                address:true,
                tax_info:false,
                payment:false,
            }
        }
    },
    mounted(){
        $('#open_balance_date').datepicker({
            format: "dd/mm/yyyy",
        }).on('changeDate',(datepicker)=>{
            this.customer_balances.open_balance_date = datepicker.target.value;
        });

        this.countries = Countries;
        this.refreshBillingCountries();
        this.refreshShippingCountries();
    },
    watch:{
        'toggleCreateModal'(a){
            if(a){
                this.clearData();this.showModal();
            }
        }
    },
    methods:{
        toggleTab(a){
            let keys = Object.keys(this.tabs)

            keys.forEach((key,index)=>{
                this.tabs[key]=false;
            });

            this.tabs[a]=true;
        },
        refreshBillingCountries(){

            $('#billing_country').selectpicker('refresh');
            $('#billing_country').on('changed.bs.select',()=>{
                this.states = [];
                this.customer_addresses.billing_state='';

                $('#billing_country').on('hidden.bs.select',function(){
                    $('#billing_state').selectpicker('refresh');
                });

                for(let u=0;u<this.countries.length;u++){
                    if(this.countries[u].country==this.customer_addresses.billing_country){
                        this.states = this.countries[u].states;
                    }
                }

                $('#billing_state').on('shown.bs.select',function(){
                    $(this).selectpicker('refresh');
                });
            });     
        },
        refreshShippingCountries(){
            $('#shipping_country').selectpicker('refresh');
            $('#shipping_country').on('changed.bs.select',()=>{
                this.states = [];
                this.customer_addresses.shipping_state='';

                $('#shipping_country').on('hidden.bs.select',function(){
                    $('#shipping_state').selectpicker('refresh');
                });

                for(let u=0;u<this.countries.length;u++){
                    if(this.countries[u].country==this.customer_addresses.shipping_country){
                        this.states = this.countries[u].states;
                    }
                }

                $('#shipping_state').on('shown.bs.select',function(){
                    $(this).selectpicker('refresh');
                });
            });
        },
        reformatBalance(){
            let u = accounting.formatNumber(this.customer_balances.open_balance,2,',','.');
            return this.customer_balances.open_balance=u;
        },
        addCompany(){
            $('.preloader').fadeIn();
            this.hideModal();
            
            this.loading = true;
            axios.post(apiUrl+'/customers/create',{
                    'customer_addresses':this.customer_addresses,
                    'customer_details':this.customer_details,
                    'customer_balances':this.customer_balances,
                    'customer_contacts':this.customer_contacts,
                })
                .then((response)=> {
                    this.loading = false;
                    this.clearData();
                    $('.preloader').fadeOut("medium",function(){
                        Swal.fire({
                          type: 'success',
                          title: 'Success',
                          html: '<span>Customer has been successfully created!</span>',
                        }).then(function(){
                            this.$emit('get-customers')
                        })
                    });
                })
                .catch((error)=> {
                    this.loading = false;
                    this.clearData();
                    $('.preloader').fadeOut("medium",function(){
                        Swal.fire({
                          type: 'error',
                          title: 'Error',
                          html: '<span>There seems to be an error in the server.</span>',
                        });
                    });
                });
        },
        showModal(){
            $('#addcustomer').modal('show');
            this.toggleTab('address');
        },
        hideModal:function(y=0){
            $('#addcustomer').modal('hide');

            if(y==1){
                this.clearData();
            }

            this.$emit('hide-modal-create-customer');
        },
        check_shipping(){
            if(!this.customer_addresses.shipping_is_same_as_billing){
                this.resetShipping().then((result) => {
                    this.clearShippingLibs()
                })
            }
        },
        clearData(){
            this.clearAllValue().then((result) => {
                this.clearAllLibs()
            })
        },
        async resetShipping(){
            this.customer_addresses.shipping_address1='';
            this.customer_addresses.shipping_address2='';
            this.customer_addresses.shipping_address3='';
            this.customer_addresses.shipping_postcode='';
            this.customer_addresses.shipping_city='';
            this.customer_addresses.shipping_state='';
            this.customer_addresses.shipping_country='';
        },
        async clearAllValue(){
            this.customer_details.customer_name='';
            this.customer_details.mobile_no='';
            this.customer_details.fax='';
            this.customer_details.email='';
            this.customer_details.website='';
            this.customer_details.phone_no='';
            this.customer_details.nickname='';
            this.customer_details.type='Company';
            this.customer_details.roc_no='';
            this.customer_details.tax_registration_no='';
            this.customer_details.default_currency='';
            this.customer_details.default_payment='';
            
            this.customer_addresses.billing_address1='';
            this.customer_addresses.billing_address2='';
            this.customer_addresses.billing_address3='';
            this.customer_addresses.billing_postcode='';
            this.customer_addresses.billing_city='';
            this.customer_addresses.billing_state='';
            this.customer_addresses.billing_country='';
            this.customer_addresses.shipping_address1='';
            this.customer_addresses.shipping_address2='';
            this.customer_addresses.shipping_address3='';
            this.customer_addresses.shipping_postcode='';
            this.customer_addresses.shipping_city='';
            this.customer_addresses.shipping_state='';
            this.customer_addresses.shipping_country='';
            this.customer_addresses.shipping_is_same_as_billing=false;

            this.customer_balances.open_balance = '0.00';
            this.customer_balances.open_balance_date = '';

            this.customer_contacts.title='';
            this.customer_contacts.first_name='';
            this.customer_contacts.last_name='';
        },
        clearAllLibs(){
            $('#add_title,#default_payment,#billing_country,#billing_state,#shipping_country,#shipping_state').selectpicker('refresh');
            $("#open_balance_date").datepicker("update","");
        },
        clearShippingLibs(){
            $('#shipping_country,#shipping_state').selectpicker();

            this.refreshShippingCountries();
        },
        hidePanels(){
            if(this.customer_details.type=='Individual'){
                if(this.tabs.tax_info==true){
                    this.tabs.tax_info = false;
                    this.tabs.address = true;
                }
            }
        }
    }
});