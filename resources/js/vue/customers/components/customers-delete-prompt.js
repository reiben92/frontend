Vue.component('customers-delete-prompt', {
    template:`
        <div class="modal fade" id="deletecustomerprompt" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Customer</h4>
                        <button type="button" class="close" v-on:click="hideModal()" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" v-if="!loading">
                                    <label class="control-label">Are you sure you want to delete this customer?</label>
                                </div>
                                <div class="form-group" v-else>
                                    <img v-bind:src="loader" style="height:15px;width:15px;display:inline-block;margin-right:5px"><span class="control-label">Processing. Please do not close this window.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-inverse" v-on:click="hideModal()">Close</button>
                        <button type="button" class="btn btn-info" v-on:click="deleteCustomer()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    props:['toggleDeleteModal','customer_data'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
        }
    },
    mounted(){
    },
    watch:{
        'toggleDeleteModal'(a){
            if(a==true){
                this.showModal();
            }
        }
    },
    methods:{
        deleteCustomer(){
            this.loading = true;

            axios.post(apiUrl+'/customers/delete',{
                    'id':this.customer_data.id,
                }).then(()=>{
                    this.hideModal();

                    Swal.fire({
                      type: 'success',
                      title: 'Success',
                      html: '<span>Customer has been successfully deleted!</span>',
                    }).then(()=>{
                        this.$emit('get-customers')
                    });
                }).catch(()=>{
                    Swal.fire({
                      type: 'error',
                      title: 'Error',
                      html: '<span>There seem to be an error on the server.</span>',
                    }).then(()=>{
                        this.$emit('get-customers')
                    });
                });
           
        },
        showModal(data){
            $('#deletecustomerprompt').modal({
                backdrop:'static',
                keyboard:false,
                show:true,
            });

            this.loading=false;
        },
        hideModal(y=0){
            this.loading=false;

            $('#deletecustomerprompt').modal('hide');
            this.$emit('hide-modal-delete-customer');
        },
    }
});