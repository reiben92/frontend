Vue.component('customers-list', {
    template:`
        <div class="page-content container-fluid">
            <div class="row mt-0 pt-0">
                <div class="col-12">
                    <div class="card-group">
                        <div class="card" style="background-color: transparent; border-right: none;">
                            <div class="pt-0">
                                <div class="d-flex align-items-center">
                                    <div style="width: 38%;">
                                        <h3 class="mb-0" style="font-weight: 500">Customers</h3>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="btn-group">
                                            <button class="btn btn-lg btn-danger" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" v-on:click="showModalCreateCustomer()">
                                                New Customer
                                            </button>
                                            <!--button type="button" class="btn btn-lg btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button-->
                                            <!--div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)">Import Customer</a>
                                            </div-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card border-top border-cyan bg-">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h2>N/A</h2>
                                    <h6 class="">Estimates</h6>
                                </div>
                                <div class="ml-auto">
                                    <span class="text-cyan display-6"><i class="ti-write"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-top border-info">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h2>N/A</h2>
                                    <h6 class="">Unbilled Activity</h6>
                                </div>
                                <div class="ml-auto">
                                    <span class="text-info display-6"><i class="ti-receipt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-top border-warning">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h2>N/A</h2>
                                    <h6 class="">Overdue</h6>
                                </div>
                                <div class="ml-auto">
                                    <span class="text-warning display-6"><i class="ti-timer"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-top border-dark">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h2>N/A</h2>
                                    <h6 class="">Open Invoices</h6>
                                </div>
                                <div class="ml-auto">
                                    <span class="text-dark display-6"><i class="ti-file"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card border-top border-success">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h2>N/A</h2>
                                    <h6 class="">Paid Last 30 Days</h6>
                                </div>
                                <div class="ml-auto">
                                    <span class="text-success display-6"><i class="ti-money"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="material-card card">
                        <customers-list-table :toggleGetCustomers='toggleGetCustomers' @show-modal-delete-customer='showModalDeleteCustomer' @disable-get-customers='disableGetCustomers'></customers-list-table>
                    </div>
                </div>
            </div>

            <customers-create-modal @hide-modal-create-customer='hideModalCreateCustomer' :toggleCreateModal='toggleCreateModal' @get-customers='enableGetCustomers'></customers-create-modal>
            <customers-delete-prompt @hide-modal-delete-customer='hideModalDeleteCustomer' :customer_data='customer_data' :toggleDeleteModal='toggleDeleteModal' @get-customers='enableGetCustomers'></customers-delete-prompt>
        </div>
    `,
    data(){
        return {
            'toggleCreateModal':false,
            'toggleDeleteModal':false,
            'toggleGetCustomers':true,
            'customer_data':{
            }
        }
    },
    methods:{
        showModalCreateCustomer(){
            this.toggleCreateModal = true;
        },
        hideModalCreateCustomer(){
            this.toggleCreateModal = false;
        },
        showModalDeleteCustomer(data){
            this.toggleDeleteModal = true;
            this.customer_data = data;
        },
        hideModalDeleteCustomer(){
            this.toggleDeleteModal = false;
            let data = {};
            this.customer_data = data;
        },
        enableGetCustomers(){
            this.toggleGetCustomers = true;
        },
        disableGetCustomers(){
            this.toggleGetCustomers = false;
        },
    },
});