Vue.component('customers-transactions', {
    template:`
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item" style="cursor:pointer"> <span class="nav-link" v-bind:class="{ active:tabs.invoices_active }" v-on:click="toggleTab('invoices_active')" role="tab"><span class="hidden-sm-up"><i class="mdi mdi-file mr-1"></i></span> <span class="hidden-xs-down">List of Issued Invoices</span></span> </li>
                            <li class="nav-item" style="cursor:pointer"> <span class="nav-link" v-bind:class="{ active:tabs.invoices }" v-on:click="toggleTab('invoices')" role="tab"><span class="hidden-sm-up"><i class="mdi mdi-file mr-1"></i></span> <span class="hidden-xs-down">List of Upcoming Invoices</span></span> </li>
                            <li class="nav-item" style="cursor:pointer"> <span class="nav-link" v-bind:class="{ active:tabs.receipts }" v-on:click="toggleTab('receipts')"><span class="hidden-sm-up"><i class="ti-money mr-1"></i></span> <span class="hidden-xs-down">List of Receipts</span></span> </li>
                            <li class="nav-item" style="cursor:pointer"> <span class="nav-link" v-bind:class="{ active:tabs.details }" v-on:click="toggleTab('details')"><span class="hidden-sm-up"><i class="ti-user mr-1"></i></span> <span class="hidden-xs-down">Customer Details</span></span> </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content py-4 px-2">
                            <div class="tab-pane" v-bind:class="{ active:tabs.invoices_active }" role="tabpanel">
                                <div class="dt-buttons container-fluid pb-3">
                                    <div class="btn-group mr-2" v-if="!invoice_checks">
                                        <button type="button" class="btn btn-outline-dark mb-3" style="margin-bottom:0!important" v-on:click="toggleInvoiceMultipleFunctions">
                                            Batch actions
                                        </button>
                                        <div class="dropdown-menu" style="display:block" v-if="toggle1">
                                            <span class="dropdown-item" style="cursor:pointer" v-on:click='enableInvoiceCheck("payment")'>Make Payment</span>
                                            <span class="dropdown-item" style="cursor:pointer" v-on:click='enableInvoiceCheck("delete")'>Delete Bulk</span>
                                        </div>
                                    </div>
                                    <div class="btn-group mr-2" v-else-if='invoice_checks && invoice_module=="payment"'>
                                        <a class="btn btn-success mr-3" v-bind:href="makePayment()" v-if="invoice_list.length>0">
                                            <i class="fa fa-check" aria-hidden="true"></i> <span>Make Payment</span>
                                        </a>
                                        <button type="button" class="btn btn-danger" v-on:click='disabledInvoiceCheck()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                    <div class="btn-group mr-2" v-else-if='invoice_checks && invoice_module=="delete"'>
                                        <button type="button" class="btn btn-success mr-3" v-if="invoice_list.length>0" v-on:click="showModalDeleteInvoicesBulk(true)">
                                            <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                        </button>
                                        <button type="button" class="btn btn-danger" v-on:click='disabledInvoiceCheck()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                    <div class="btn-group mr-2 pull-right" v-if="!invoice_module">
                                        <a v-bind:href="createInvoice()" class="btn btn-success ">
                                            <span>Create Invoice</span>
                                        </a>
                                    </div>
                                </div>
                                <invoices-list :toggleModalDeleteInvoicesBulk='toggleModalDeleteInvoicesBulk' @show-modal-delete-invoices-bulk='showModalDeleteInvoicesBulk' :args='args_invoices_1' :getInvoice='getInvoice' @update-parent-invoices='updateParentInvoices' @set-get-invoice='setGetInvoice' :checks='invoice_checks' @disable-checks='disabledInvoiceCheck'></invoices-list>
                                <div class="dt-buttons" v-if='invoice_checks && invoice_module=="payment"'>
                                    <div class="btn-group mr-2 pull-right">
                                        <a class="btn btn-success mb-3 mr-3" v-bind:href="makePayment()" v-if="invoice_list.length>0">
                                            <i class="fa fa-check" aria-hidden="true"></i> <span>Make Payment</span>
                                        </a>
                                        <button type="button" class="btn btn-danger mb-3" v-on:click='disabledInvoiceCheck()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="dt-buttons" v-else-if='invoice_checks && invoice_module=="delete"'>
                                    <div class="btn-group mr-2 pull-right">
                                        <button type="button" class="btn btn-success mb-3 mr-3" v-if="invoice_list.length>0" v-on:click='showModalDeleteInvoicesBulk(true)'>
                                            <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                        </button>
                                        <button type="button" class="btn btn-danger mb-3" v-on:click='disabledInvoiceCheckDraft()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" v-bind:class="{ active:tabs.invoices }" role="tabpanel">
                                <div class="dt-buttons container-fluid pb-3">
                                    <div class="btn-group mr-2" v-if="!invoice_checks_draft">
                                        <button type="button" class="btn btn-outline-dark mb-3" style="margin-bottom:0!important" v-on:click="toggleInvoiceDraftMultipleFunctions">
                                            Batch actions
                                        </button>
                                        <div class="dropdown-menu" style="display:block" v-if="toggle2">
                                            <span class="dropdown-item" style="cursor:pointer" v-on:click='enableInvoiceCheckDraft("delete")'>Delete Bulk</span>
                                        </div>
                                    </div>
                                    <div class="btn-group mr-2" v-if='invoice_checks_draft && invoice_module_draft=="delete"'>
                                        <button type="button" class="btn btn-success mr-3" v-if="invoice_list.length>0" v-on:click="showModalDeleteInvoicesBulk(true)">
                                            <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                        </button>
                                        <button type="button" class="btn btn-danger" v-on:click='disabledInvoiceCheckDraft()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                    <div class="btn-group mr-2 pull-right" v-if="!invoice_module_draft">
                                        <a v-bind:href="createInvoice()" class="btn btn-success ">
                                            <span>Create Invoice</span>
                                        </a>
                                    </div>
                                </div>
                                <invoices-list :toggleModalDeleteInvoicesBulk='toggleModalDeleteInvoicesBulk' @show-modal-delete-invoices-bulk='showModalDeleteInvoicesBulk' :args='args_invoices_2' :getInvoice='getInvoice' @update-parent-invoices='updateParentInvoices' @set-get-invoice='setGetInvoice' :checks='invoice_checks_draft' @disable-checks='disabledInvoiceCheckDraft'></invoices-list>
                                <div class="dt-buttons" v-if='invoice_checks_draft && invoice_module_draft=="delete"'>
                                    <div class="btn-group mr-2 pull-right">
                                        <button type="button" class="btn btn-success mb-3 mr-3" v-if="invoice_list.length>0" v-on:click='showModalDeleteInvoicesBulk(true)'>
                                            <i class="fa fa-trash" aria-hidden="true"></i> <span>Delete</span>
                                        </button>
                                        <button type="button" class="btn btn-danger mb-3" v-on:click='disabledInvoiceCheckDraft()'>
                                            <i class="fa fa-ban" aria-hidden="true"></i> <span>Cancel</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" v-bind:class="{ active:tabs.receipts }" role="tabpanel">
                                <receipts-list :args='args'></receipts-list>
                            </div>
                            <div class="tab-pane" v-bind:class="{ active:tabs.details }" role="tabpanel">
                                <div class="card pb-0 mb-0">
                                    <hr class="mt-0">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Customer:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.customer_name'> {{ customer_details.customer_name }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Email:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.email'>  {{ customer_details.email }}  </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row" v-if="customer_details.type=='Company'">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Tax Registration Number:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.tax_registration_no'> {{ customer_details.tax_registration_no }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Company Registration: Number</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.roc_no'> {{ customer_details.roc_no }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Billing Address:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='returnCustomerAddressBilling'> {{ returnCustomerAddressBilling }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Shipping Address:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='returnCustomerAddressShipping'> {{ returnCustomerAddressShipping }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Mobile:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.mobile_no'> {{ customer_details.mobile_no }}</p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Fax:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if="customer_details.fax">{{ customer_details.fax }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Phone:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.phone_no'>  {{ customer_details.phone_no }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Website:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.website'>  {{ customer_details.website }} </p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Preferred Payment Method:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static" v-if='customer_details.default_payment'> {{ customer_details.default_payment }}</p>
                                                        <p class="form-control-static" v-else> N/A </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-4">Preferred Delivery Method:</label>
                                                    <div class="col-md-8">
                                                        <p class="form-control-static"> N/A  </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-0 mb-5">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ renderCustomerData }}
        </div>
    `,
    props:['customer_data','getInvoice'],
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            customer:{},
            args:{
                customer_id:'',
            },
            args_invoices_1:{
                customer_id:'',
            },
            args_invoices_2:{
                customer_id:'',
                invoice_status_id:1,
            },
            customer:{
                id:'',
            },
            customer_details:{
                id:'',
                customer_id:'',
                customer_name:'',
                mobile_no:'',
                fax:'',
                email:'',
                website:'',
                phone_no:'',
                nickname:'',
                type:'Company',
                roc_no:'',
                tax_registration_no:'',
                default_currency:'',
                default_payment:'',
            },
            customer_contacts:{
                id:'',
                first_name:'',
                last_name:'',
                title:'',
                customer_id:'',
            },
            customer_balances:{
                id:'',
                customer_id:'',
                current_balance:'0.00',
                current_balance_date:'',
            },
            customer_addresses:{
                id:'',
                customer_id:'',
                billing_address1:'',
                billing_address2:'',
                billing_address3:'',
                billing_city:'',
                billing_postcode:'',
                billing_state:'',
                billing_country:'',

                shipping_address1:'',
                shipping_address2:'',
                shipping_address3:'',
                shipping_city:'',
                shipping_postcode:'',
                shipping_state:'',
                shipping_country:'',
                shipping_is_same_as_billing:false,
            },
            tabs:{
                invoices_active:true,
                invoices:false,
                receipts:false,
                details:false,
            },
            invoice_checks:false,
            invoice_checks_draft:false,
            toggle1:false,
            toggle2:false,
            hasOpened1:false,
            hasOpened2:false,
            invoice_module:'',
            invoice_module_draft:'',
            toggleModalDeleteInvoicesBulk:false,
            invoice_list:[],
        }
    },
    mounted(){
        let url = window.location.href;
        this.args.customer_id = url[url.length-1];
        this.args_invoices_1.customer_id = url[url.length-1];   
        this.args_invoices_2.customer_id = url[url.length-1];      
    },
    computed:{
        renderCustomerData(){
            if(this.customer_data.id){
                this.init(this.customer_data);
            }

        },
        returnCustomerAddressBilling(){
            let u = [];
            let keys = ['billing_address1','billing_address2','billing_address3','billing_city','billing_postcode','billing_state','billing_country'];

            keys.forEach((item,index)=>{
                if(this.customer_addresses[item]){
                    u.push(this.customer_addresses[item]);
                }
            });

            return u.join();
        },
        returnCustomerAddressShipping(){
            let u = [];
            let keys = ['shipping_address1','shipping_address2','shipping_address3','shipping_city','shipping_postcode','shipping_state','shipping_country'];

            keys.forEach((item,index)=>{
                if(this.customer_addresses[item]){
                    u.push(this.customer_addresses[item]);
                }
            });

            return u.join();
        }
    },
    methods:{
        showModalDeleteInvoicesBulk(a){
            this.toggleModalDeleteInvoicesBulk=a;
        },
        updateParentInvoices(a){
            this.invoice_list = a;
        },
        setGetInvoice(a){
            this.$emit('set-get-invoice',a);
        },
        createInvoice(){
            return baseUrl+'/invoice/create?customer_id='+this.customer.id;
        },
        toggleTab(a){
            let keys = Object.keys(this.tabs)

            keys.forEach((key,index)=>{
                this.tabs[key]=false;
            });

            if(a=='invoices' || a=='invoices_active'){
                this.$emit('set-get-invoice',true);
            }
            else if(a=='details'){
                this.$emit('get-customer');
            }

            this.tabs[a]=true;
        },
        init(data){
            this.initData(data).then(()=>{
            }); 
        },
        async initData(data){
            if(data.id){
                this.customer.id = data.id;
            }

            if(data.customer_details){
                for(let i=0;i<data.customer_details.length;i++){
                    this.customer_details = data.customer_details[i];
                }
            }

            if(data.customer_addresses){
                for(let i=0;i<data.customer_addresses.length;i++){
                    this.customer_addresses = data.customer_addresses[i];
                }
            }

            if(data.customer_contacts){
                for(let i=0;i<data.customer_contacts.length;i++){
                    this.customer_contacts = data.customer_contacts[i];
                }
            }
        },
        enableInvoiceCheck(title){
            this.invoice_checks = true;
            this.invoice_module = title;

            if(this.invoice_module=='payment'){

            }
            else if(this.invoice_module=='delete'){

            }
        },
        disabledInvoiceCheck(){
            this.invoice_checks = false;
            this.invoice_module = '';
        },
        enableInvoiceCheckDraft(title){
            this.invoice_checks_draft = true;
            this.invoice_module_draft = title;

        },
        disabledInvoiceCheckDraft(){
            this.invoice_checks_draft = false;
            this.invoice_module_draft = '';
        },
        async clickOutside(){
            if(!this.toggle1){
                $(document).ready(()=>{
                    $('body').click(()=>{
                        if(this.hasOpened1){
                            this.toggle1=false;
                            this.hasOpened1=false;
                            $('body').unbind('click');
                        }
                    });
                });
            }
        },
        toggleInvoiceMultipleFunctions(){
            this.clickOutside().then(()=>{
                this.toggle1=true;
                this.hasOpened1 = true;
            });
        },
        async clickOutsideDrafts(){
            if(!this.toggle2){
                $(document).ready(()=>{
                    $('body').click(()=>{
                        if(this.hasOpened2){
                            this.toggle2=false;
                            this.hasOpened2=false;
                            $('body').unbind('click');
                        }
                    });
                });
            }
        },
        toggleInvoiceDraftMultipleFunctions(){
            this.clickOutsideDrafts().then(()=>{
                this.toggle2=true;
                this.hasOpened2 = true;
            });
        },
        makePayment(){
            let url = baseUrl+'/receipt/create?customer_id='+this.customer.id;

            if(this.customer.id){
                let arr = '';

                let invoice_id = [];

                for(let i=0;i<this.invoice_list.length;i++){
                    invoice_id.push(this.invoice_list[i].id);
                }

                if(invoice_id.length>0){
                    arr = invoice_id.join();
                    url += '&invoice_id='+arr;
                }
            }

            return url;
        }
    }
});