Vue.component('customers-view', {
    template:`
        <div class="page-content container-fluid">
            <div v-if='loaded'>
                <div class="row mt-0 pt-0">
                    <div class="col-12">
                        <div class="card-group">
                            <div class="card" style="background-color: transparent; border-right: none;">
                                <div class="pt-0">
                                    <div class="d-flex align-items-center">
                                        <div style="width: 38%;">
                                            <h3 class="mb-0" style="font-weight: 500">{{ customer_details.customer_name }}</h3>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="btn-group">
                                                <button class="btn btn-lg btn-danger mr-2" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" v-on:click="toggleModalEditCustomer"><i class="mdi mdi-pencil"></i> Edit Details
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h4>{{ customer_details.customer_name }}</h4>
                                        <h6 class="mt-3"><i class="mdi mdi-map-marker mr-2"></i>{{ returnCustomerAddressBilling }}</h6>
                                        <h6 class=""><i class="mdi mdi-email mr-2"></i>{{ customer_details.email }}</h6>
                                        <h6 class=""><i class="mdi mdi-phone mr-2"></i>{{ customer_details.phone_no }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card border-top border-dark pb-3">
                            <div class="card-body mt-4">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2>N/A</h2>
                                        <h6 class="">Open Invoices</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <span class="text-dark display-6"><i class="ti-file"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card border-top border-warning pb-3">
                            <div class="card-body mt-4">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2>N/A</h2>
                                        <h6 class="">Overdue</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <span class="text-warning display-6"><i class="ti-timer"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <customers-transactions :getInvoice='getInvoice' @set-get-invoice='setGetInvoice' :customer_data='customer_data' @get-customer='getCustomer'></customers-transactions>
            <customers-edit-modal :toggleEditModal='toggleEditModal' :loadedCustomerData='loadedCustomerData' @set-get-invoice='setGetInvoice' @unload-customer-data='unloadCustomerData' :customer_data='customer_data' @get-customer='getCustomer' @hide-modal-edit-customer='hideModalEditCustomer'></customers-edit-modal>
        </div>
    `,
    data(){
    return {
            loader:baseUrl+'/img/loader.gif',
            loading:false,
            customer:{
                id:'',
            },
            customer_details:{
                id:'',
                customer_id:'',
                customer_name:'',
                mobile_no:'',
                fax:'',
                email:'',
                website:'',
                phone_no:'',
                nickname:'',
                type:'Company',
                roc_no:'',
                tax_registration_no:'',
                default_currency:'',
                default_payment:'',
            },
            customer_contacts:{
                id:'',
                first_name:'',
                last_name:'',
                title:'',
                customer_id:'',
            },
            customer_balances:{
                id:'',
                customer_id:'',
                current_balance:'0.00',
                current_balance_date:'',
            },
            customer_addresses:{
                id:'',
                customer_id:'',
                billing_address1:'',
                billing_address2:'',
                billing_address3:'',
                billing_city:'',
                billing_postcode:'',
                billing_state:'',
                billing_country:'',

                shipping_address1:'',
                shipping_address2:'',
                shipping_address3:'',
                shipping_city:'',
                shipping_postcode:'',
                shipping_state:'',
                shipping_country:'',
                shipping_is_same_as_billing:false,
            },
            loaded:false,
            original_data:{},
            toggleEditModal:false,

            loadedCustomerData:false,
            customer_data:{},
            getInvoice:false,
        }
    },
    computed:{
        returnCustomerAddressBilling(){
            let u = [];
            let keys = ['billing_address1','billing_address2','billing_address3','billing_city','billing_postcode','billing_state','billing_country'];

            keys.forEach((item,index)=>{
                if(this.customer_addresses[item]){
                    u.push(this.customer_addresses[item]);
                }
            });

            return u.join();
        },
        returnCustomerAddressShipping(){
            let u = [];
            let keys = ['shipping_address1','shipping_address2','shipping_address3','shipping_city','shipping_postcode','shipping_state','shipping_country'];

            keys.forEach((item,index)=>{
                if(this.customer_addresses[item]){
                    u.push(this.customer_addresses[item]);
                }
            });

            return u.join();
        }
    },
    mounted(){
        let $this = this;

        let url = window.location.href;

        this.customer.id = url[url.length-1];

        this.getCustomer();
    },
    methods:{
        setGetInvoice(a){
            this.getInvoice=a;
        },
        unloadCustomerData(){
            this.loadedCustomerData=false;
        },
        hideModalEditCustomer(){
            this.toggleEditModal = false;
        },
        returnNewInvoice(){
            return baseUrl+'/invoice/create?customer_id='+this.customer.id;
        },
        returnNewPayment(){
            return baseUrl+'/receipt/create?customer_id='+this.customer.id;
        },
        toggleModalEditCustomer(){
            this.getCustomer().then(()=>{
                //VueApp.$emit('toggle-modal-edit-customer');
                this.toggleEditModal=true;
            });
        },
        async getCustomer(){
            axios.post(apiUrl+'/customers/get',{
                    'id':this.customer.id,
                })
                .then((response)=> {
                    if(response.data.data){
                        this.customers = response.data.data;
                        this.loaded = true;
                        this.initData(response.data.data);
                        this.customer_data = response.data.data;
                        this.loadedCustomerData = true;
                    }
                    else{
                        Swal.fire({
                          type: 'error',
                          title: '404',
                          html: '<span>Customer not found.</span>',
                        });
                    }
                });
        },
        async initData(data){
            for(let i=0;i<data.customer_details.length;i++){
                this.customer_details = data.customer_details[i];
            }

            for(let i=0;i<data.customer_addresses.length;i++){
                this.customer_addresses = data.customer_addresses[i];
            }
        }
    }
});