@extends('master')
@section('content')

<div class="page-wrapper" style="display: block;">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">PRODUCTS & SERVICES</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Products & Services</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="page-content container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row mt-0 pt-0">
            <div class="col-12">
                <div class="card-group">
                    <div class="card" style="background-color: transparent; border-right: none;">
                        <div class="pt-0">
                            <div class="d-flex align-items-center">
                                <div style="width: 38%;">
                                    <h3 class="mb-0" style="font-weight: 500">Products & Services</h3>
                                </div>
                                <div class="ml-auto">
                                    <div class="btn-group">
                                        <a href="{{ route('product_services.create') }}">
                                            <button class="btn btn-lg btn-danger" type="button" aria-haspopup="true" aria-expanded="false">
                                                New Product/Service
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="dt-buttons mb-2 mt-1">
                            <!--div class="btn-group mr-4">
                                <button type="button" class="btn btn-outline-dark dropdown-toggle mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Batch actions
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="javascript:void(0)">Send Invoices</a>
                                        <a class="dropdown-item" href="javascript:void(0)">Print Invoices</a>
                                        <a class="dropdown-item" href="javascript:void(0)">Send Reminders</a>
                                    </div>
                                </button>
                            </div-->
                        </div>
                        <product-services-list :args='args'></product-services-list>               
                    </div>
                </div>
            </div>
        </div>
        <product-services-delete-prompt></product-services-delete-prompt>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
</div>

@endsection
@section('js')
@parent

<script type="text/javascript" src="{{ asset(mix('/js/product-services-list.js')) }}"></script>


@endsection