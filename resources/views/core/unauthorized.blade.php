@extends('master')
@section('content')

<div class="page-wrapper" style="display: block;">
    
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="page-content container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                        <div class="card-body">
                            <div class="col-md-12 m-b-10">
                        </div>
                        <div class="col-sm-12 m-t-10">
                            <div class="white-box">
                                 <div id="notfound">
                                    <div class="notfound">
                                        <div class="notfound-404">
                                            <h1>512</h1>
                                        </div>
                                        <h2>Oops! You Are Not Allowed</h2>
                                        <p>You are not authorized to view this application. Please contact the local administrator for this application. <a href="{{ ENV('SSO_URL') }}">Return to homepage</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent



@endsection