@extends('master')
@section('content')

<div class="page-wrapper" style="display: block;">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Dashboard</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="page-content container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Overdue</h5>
                        <div class="text-right">
                            <span class="text-muted">Last 365 Days</span>
                            <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-warning"></i></sup>RM380,000.00</h2>
                        </div>
                        <span class="text-warning">90%</span>
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Not Due Yet</h5>
                        <div class="text-right">
                            <span class="text-muted">Last 365 Days</span>
                            <h2 class="mt-2 display-7"><sup><i class="ti-arrow-down text-inverse"></i></sup>RM10,000.00</h2>
                        </div>
                        <span class="text-inverse">10%</span>
                        <div class="progress">
                            <div class="progress-bar bg-inverse" role="progressbar" style="width: 10%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Not Deposited</h5>
                        <div class="text-right">
                            <span class="text-muted">Last 30 Days</span>
                            <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-info"></i></sup>RM0.00</h2>
                        </div>
                        <span class="text-info">0%</span>
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Deposited</h5>
                        <div class="text-right">
                            <span class="text-muted">Last 30 Days</span>
                            <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-success"></i></sup>RM580,000.00</h2>
                        </div>
                        <span class="text-success">90%</span>
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 90%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-body pb-5">
                        <div class="d-flex align-items-center">
                            <div>
                                <h4 class="card-title text-uppercase">Sales</h4>
                                <h5 class="card-subtitle mb-0">Last Month</h5>
                            </div>
                            <div class="ml-auto">
                                <select class="form-control">
                                    <option>Last Month</option>
                                    <option>February 2018</option>
                                    <option>March 2018</option>
                                </select>
                            </div>
                        </div>
                        <div class="mt-3" id="morris-line-chart" style="height: 390px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                            <svg height="390" version="1.1" width="631.875" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.1125px; top: -0.3375px;">
                                <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc>
                                <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                <text x="49.21875" y="351.375" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                </text>
                                <path fill="none" stroke="#eef0f2" d="M61.71875,351.375H606.875" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="49.21875" y="269.78125" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">5,000</tspan>
                                </text>
                                <path fill="none" stroke="#eef0f2" d="M61.71875,269.78125H606.875" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="49.21875" y="188.1875" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">10,000</tspan>
                                </text>
                                <path fill="none" stroke="#eef0f2" d="M61.71875,188.1875H606.875" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="49.21875" y="106.59375" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000</tspan>
                                </text>
                                <path fill="none" stroke="#eef0f2" d="M61.71875,106.59375H606.875" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="49.21875" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">20,000</tspan>
                                </text>
                                <path fill="none" stroke="#eef0f2" d="M61.71875,25H606.875" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="506.8524073511543" y="363.875" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,6.8125)">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan>
                                </text>
                                <text x="264.413540400972" y="363.875" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,6.8125)">
                                    <tspan dy="4.3984375" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan>
                                </text>
                                <path fill="none" stroke="#2962ff" d="M61.71875,307.8692125C76.95397934386392,307.4122875,107.42443803159173,310.6230015625,122.65966737545565,306.0415125C137.89489671931958,301.4600234375,168.3653554070474,273.24572954234975,183.6005847509113,271.2173C198.67021377582017,269.2109186048497,228.8094718256379,293.7738921875,243.87910085054676,289.90226875C258.9487298754556,286.03064531250004,289.0879879252734,244.10492301058744,304.1576169501823,240.2443125C319.3928462940462,236.34127769808742,349.863304981774,254.7883984375,365.0985343256379,258.8476875C380.3337636695018,262.9069765625,410.8042223572296,292.00408808060104,426.0394517010935,272.718625C441.1090807260024,253.64278651810105,471.2483387758202,117.43636477037293,486.31796780072904,105.40248125C501.22199650668284,93.50083820787293,531.0300539185905,163.57878016826925,545.9340826245443,176.97651875C561.1693119684082,190.67198485576924,591.6397706561361,204.5756046875,606.875,213.7753" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <circle cx="61.71875" cy="307.8692125" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="122.65966737545565" cy="306.0415125" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="183.6005847509113" cy="271.2173" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="243.87910085054676" cy="289.90226875" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="304.1576169501823" cy="240.2443125" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="365.0985343256379" cy="258.8476875" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="426.0394517010935" cy="272.718625" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="486.31796780072904" cy="105.40248125" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="545.9340826245443" cy="176.97651875" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="606.875" cy="213.7753" r="4" fill="#2962ff" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                            </svg>
                            <div class="morris-hover morris-default-style" style="left: 499.572px; top: 108px;">
                                <div class="morris-hover-row-label">2013 Q1</div>
                                <div class="morris-hover-point" style="color: #2962FF">
                                    Item 1: 10,687
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Expenses</h5>
                        <div class="mt-3">
                            <div id="morris-donut-chart" style="height:350px; padding-top: 50px;">
                                <svg height="300" version="1.1" width="631.875" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.2125px; top: -0.5375px;">
                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc>
                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                    <path fill="none" stroke="#ff7676" d="M315.9375,243.33333333333331A93.33333333333333,93.33333333333333,0,0,0,367.78965287789873,227.60454466742348" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                    <path fill="#ff7676" stroke="#ffffff" d="M315.9375,246.33333333333331A96.33333333333333,96.33333333333333,0,0,0,369.4563292204026,230.09897646030495L390.93793541267496,262.24943067966615A135,135,0,0,1,315.9375,285Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <path fill="none" stroke="#2cabe3" d="M367.78965287789873,227.60454466742348A93.33333333333333,93.33333333333333,0,0,0,402.1652724453207,185.7194956603408" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                    <path fill="#2cabe3" stroke="#ffffff" d="M369.4563292204026,230.09897646030495A96.33333333333333,96.33333333333333,0,0,0,404.9368794167774,186.86762230656603L440.6598137155531,201.66569908013582A135,135,0,0,1,390.93793541267496,262.24943067966615Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <path fill="none" stroke="#53e69d" d="M402.1652724453207,185.7194956603408A93.33333333333333,93.33333333333333,0,0,0,373.20131672409553,76.29812481829035" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                    <path fill="#53e69d" stroke="#ffffff" d="M404.9368794167774,186.86762230656603A96.33333333333333,96.33333333333333,0,0,0,375.04193940451285,73.92913597316397L398.765520618781,43.39550196931282A135,135,0,0,1,440.6598137155531,201.66569908013582Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <path fill="none" stroke="#7bcef3" d="M373.20131672409553,76.29812481829035A93.33333333333333,93.33333333333333,0,1,0,315.9081784690487,243.333328727518" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path>
                                    <path fill="#7bcef3" stroke="#ffffff" d="M375.04193940451285,73.92913597316397A96.33333333333333,96.33333333333333,0,1,0,315.9072359912682,246.3333285794739L315.89351770357314,289.999993091277A140,140,0,1,1,401.8332250861433,39.44718722743552Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="315.9375" y="140" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;" font-weight="800" transform="matrix(2.158,0,0,2.158,-366.171,-174.6403)" stroke-width="0.4633928571428571">
                                        <tspan dy="6" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Apr</tspan>
                                    </text>
                                    <text x="315.9375" y="160" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;" transform="matrix(1.9425,0,0,1.9425,-297.8892,-143.2672)" stroke-width="0.5147879464285714">
                                        <tspan dy="4.8046875" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">105</tspan>
                                    </text>
                                </svg>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mt-4">
                            <div>
                                <h3 class="font-medium text-uppercase">Total Expenses</h3>
                                <h5 class="text-muted">Last month</h5>
                            </div>
                            <div class="ml-auto">
                                <button class="btn btn-info btn-circle btn-lg text-white">
                                    <i class="ti-money"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
</div>

@endsection
@section('js')
@parent

<script type="text/javascript" src="{{ asset(mix('/js/dashboard.js')) }}"></script>

@endsection