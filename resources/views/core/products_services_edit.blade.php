@extends('master')
@section('content')

<div class="page-wrapper" style="display: block;">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">PRODUCTS & SERVICES</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('product_services') }}">Product & Services</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Product & Service</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <product-services-edit></product-services-edit>
</div>

@endsection
@section('js')
@parent

<script type="text/javascript" src="{{ asset(mix('/js/product-services-edit.js')) }}"></script>


@endsection