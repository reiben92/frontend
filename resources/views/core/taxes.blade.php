@extends('master')
@section('content')

<div class="page-wrapper" style="display: block;">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Taxes</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Taxes</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="page-content container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row mt-0 pt-0">
            <div class="col-12">
                <div class="card-group">
                    <div class="card" style="background-color: transparent; border-right: none;">
                        <div class="pt-0">
                            <div class="d-flex align-items-center">
                                <div style="width: 38%;">
                                    <h3 class="mb-0" style="font-weight: 500">Taxes</h3>
                                </div>
                                <div class="ml-auto">
                                    <div class="btn-group">
                                        <button class="btn btn-lg btn-danger" type="button" aria-haspopup="true" aria-expanded="false" v-on:click='toggleAddTaxes()'>
                                            New Taxes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="dt-buttons mb-2 mt-1">
                        </div>
                        <taxes-list :args='args'></taxes-list>               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent

<script type="text/javascript" src="{{ asset(mix('/js/taxes.js')) }}"></script>


@endsection