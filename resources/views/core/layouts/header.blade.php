<header class="topbar" data-navbarbg="skin1">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark" >
        <div class="navbar-header" data-logobg="skin5" style="padding-top:1px">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="{{ route('index') }}"><i class="ti-menu ti-close"></i></a>
            <a class="navbar-brand d-block d-md-none" href="{{ route('index') }}">
                <!-- Logo icon -->
                <b class="logo-icon">
                    <!-- Dark Logo icon -->
                    <img src="/assets/images/logos/logo-icon.png" alt="homepage" class="dark-logo">
                    <!-- Light Logo icon -->
                    <img src="/assets/images/logos/logo-light-icon.png" alt="homepage" class="light-logo">
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="logo-text">
                        <!-- dark Logo text -->
                        <img src="/assets/images/logos/logo-text.png" alt="homepage" class="dark-logo">
                        <!-- Light Logo text -->    
                        <img src="/assets/images/logos/logo-light-text.png" class="light-logo" alt="homepage">
                </span>
            </a>
            <div class="d-none d-md-block text-center">
                <a class="sidebartoggler waves-effect waves-light d-flex align-items-center side-start" href="{{ route('index') }}" data-sidebartype="mini-sidebar">
                    <i class="mdi mdi-menu"></i>
                    <span class="navigation-text ml-3"> Navigation</span>
                </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="#" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin1">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-left mr-auto">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <li class="nav-item border-right">
                    <a class="nav-link navbar-brand d-none d-md-block" href="{{ route('index') }}">
                        <!-- Logo icon -->
                        <b class="logo-icon mr-2">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Light Logo icon -->
                            <img src="/assets/images/logos/logo-light-icon.png" alt="homepage" class="light-logo" style="height: 30px;">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                                <!-- Light Logo text -->    
                                <img src="/assets/images/logos/logo-light-text.png" class="light-logo" alt="homepage" style="height: 30px;">
                        </span>
                    </a>
                </li>
                <!-- ============================================================== -->
            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-right">
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <?php if(!empty(Auth::user())){ ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="/assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                        <span class="ml-2 user-text font-medium">{{ Auth::user()->name }}</span><span class="fas fa-angle-down ml-2 user-text"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                            <div class=""><img src="/assets/images/users/1.jpg" alt="user" class="rounded" width="80"></div>
                            <div class="ml-2">
                                <h4 class="mb-0">{{ Auth::user()->name }}</h4>
                                <p class=" mb-0 text-muted">{{ Auth::user()->email }}</p>
                                <a href="#" class="btn btn-sm btn-danger text-white mt-2 btn-rounded">View Profile</a>
                            </div>
                        </div>
                        <a class="dropdown-item" href="{{ route('profile') }}"><i class="ti-user mr-1 ml-1"></i> My Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>