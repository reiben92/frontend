<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar ps-container ps-theme-default ps-active-y" data-ps-id="fe23fdc0-a235-9a19-53d9-4b4c376f1481">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('index') }}" aria-expanded="false">
                        <i class="mdi mdi-av-timer"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('invoice') }}" aria-expanded="false">
                        <i class="mdi mdi-file-document"></i><span class="hide-menu">Invoice</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('customer') }}" aria-expanded="false">
                        <i class="mdi mdi-account-card-details"></i><span class="hide-menu">Customer</span>
                    </a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('product_services') }}" aria-expanded="false">
                        <i class="mdi mdi-cart"></i><span class="hide-menu">Product & Services</span>
                    </a>
                </li>
            </ul>
        </nav>
    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 0px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
</aside>