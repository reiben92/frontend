<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="baseUrl" content="{{ env('APP_URL') }}">
    <meta name="apiUrl" content="{{ env('API_URL') }}">
    <meta name="hostName" content="{{ env('HOSTNAME') }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/favicon.png') }}">
    <title>IX Telecom - Billing</title>
    <!-- Custom CSS -->
    <link href="{{ asset('/dist/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/js/pages/chartist/chartist-init.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/extra-libs/jvector/jvector.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <!--morris CSS -->
    <link href="{{ asset('/assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js") }}'></script>
    <script type='text/javascript' src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js") }}'></script>
<![endif]-->
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
    .displayNone{display: none!important}
</style></head>