<html dir="ltr" lang="en">
    @include('core.layouts.head')
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
   
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin1" data-sidebartype="mini-sidebar" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        @include('core.layouts.header')

        <div class="left-sidebar" data-sidebarbg="skin5">
            <sidebar></sidebar>
        </div>
        @yield('content')
        @include('core.layouts.footer')
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <script type='text/javascript' src='{{ asset(mix("/js/app.js")) }}'></script>
    <!-- apps -->
    <script type='text/javascript' src='{{ asset("/dist/js/app.min.js") }}'></script>
    <script type='text/javascript' src='{{ asset("/dist/js/app.init.mini-sidebar.js") }}'></script>
    <script type='text/javascript' src='{{ asset("/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js") }}'></script>
    
    
    <style>
    .btn-picker .btn{
        color: #525f7f;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #e9ecef;
        box-shadow: none;
    }

    .v-t-m{
        vertical-align: middle!important
    }

    .fs-12{
        font-size:12px!important;
    }

    .fs-14{
        font-size:14px!important;
        white-space: normal!important;
    }

    .fs-16{
        font-size:16px!important;
    }


    </style>
</body>
    <!--This page plugins -->
    @yield('js')
</body>
</html>