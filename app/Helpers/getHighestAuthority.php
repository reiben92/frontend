<?php

namespace App\Helpers;

use App\Models\Users;

class getHighestAuthority
{
    protected $token;

    public $users;
    public $counter = 0;

    public function __construct($users){
        $this->users = $users;
        $this->getBoss();
    }

    public function getBoss(){
        if($this->counter>=100){
            return ['Exceed number of tries'];
        }

        else{
            $this->users = Users::where('user_id',$this->users->reports_to)->first();

            if(!empty($this->users->reports_to)){
                $this->counter++;
                $this->getBoss();
            }
            else{
                return $this->users;
            }
        }
    }
}
