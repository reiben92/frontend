<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class ValidateUser
{
    protected $token;

    public function __construct($token){
        $this->token = $token;
    }

    public function authenticate(){
        $http = new Client;
        $code = 404;

        $url = env('SSO_URL').'/api/auth/user';

        $headers = ['Accept' => 'application/json',
                    'Authorization' => 'Bearer '.$this->token,
                    ];

        $response = $http->post($url,[
                        'headers'=> $headers,
                        'http_errors' => false,
                    ]);

        $data = json_decode($response->getBody(),true);
        
        if(!empty($data['data'])){
            $code = 200;
            $data = $data['data'];
        }

        return ['data'=>$data,'code'=>$code];
    }
}
