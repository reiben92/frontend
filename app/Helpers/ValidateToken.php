<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class ValidateToken
{
    protected $token;

    public function __construct($token){
        $this->token = $token;
    }

    public function validate(){
        $http = new Client;

        $data = [];
        $code = 404;

        $url = env('SSO_URL').'/api/auth/validate';

        $headers = ['Accept' => 'application/json',
                    'Authorization' => 'Bearer '.$this->token,
                    ];

        $response = $http->post($url,[
                        'headers'=> $headers,
                        'http_errors' => false,
                    ]);

        $data = json_decode($response->getBody(),true);

        if($data['message']=='Token is valid.'){
            return 200;
        }

        return 404;

    }
}
