<?php

namespace App\Http\Middleware;

use Closure;
use Session;

use App\Helpers\ValidateToken;

class Authenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        
    	if($request->session()->has('access_token') ){
            //print_r($request->session()->get('access_token'));exit;
    		$ValidateToken = new ValidateToken($request->session()->get('access_token'));
    		
    		if($ValidateToken->validate()==200){
				return $next($request);
			}
            else{
                return redirect()->route('sso.redirect');
            }
		}
        else{
            return redirect()->route('sso.redirect');
        }
    	//return redirect()->route('logout');
    }
}