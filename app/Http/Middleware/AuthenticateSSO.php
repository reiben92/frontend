<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ValidateUser;
use App\Helpers\ValidateToken;
use App\Models\UsersAuth;
use Closure;

class AuthenticateSSO
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next)
    {   
        if(env('SSO_MODE')){
            if($request->session()->has('access_token')){
                $ValidateToken = new ValidateToken($request->session()->get('access_token'));

                //print_r($ValidateToken);exit;

                if($ValidateToken->validate()==200){
                    $ValidateUser = new ValidateUser($request->session()->get('access_token'));
                    $data = $ValidateUser->authenticate();

                    if($data['code']==200){
                        $User = UsersAuth::where('email',$data['data']['email'])->first();
                        if(!empty($User)){
                            if(Auth::check()){
                                if(Auth::user()->email != $data['data']['email']){
                                    Auth::login($User);
                                    return $next($request);
                                }
                                else{
                                    return $next($request);
                                }
                            }
                            else{
                                Auth::login($User);
                                return $next($request);
                            }
                        }
                        else{
                            return redirect()->route('unauthorized');
                        }
                    }
                }
                else{
                    return redirect()->route('sso.redirect');
                }
            }
            else{
                return redirect()->route('sso.redirect');
            }
        }
        else{
            if(Auth::check()){
                return $next($request);
            }
            else{
                return redirect()->route('login');
            }
        }
    }
}