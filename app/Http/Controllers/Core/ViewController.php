<?php

namespace App\Http\Controllers\Core;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Session;


class ViewController extends Controller{
    public function index(Request $request){
        
        $client = new \GuzzleHttp\Client();
        $url = env('API_URL').'/authenticate';
        $args = ['client_id'=>1,'client_secret'=>'password'];

        try{
            $response = $client->post($url,['form_params'=>$args]);

            $result = $response->getBody()->getContents();

            if($result){
                $data =  json_decode($result,true);
                if(empty(url()->previous()) || (strpos(url()->previous(), env('APP_URL')) === false )){
                    $data['url'] = route('dashboard');
                }
                else{
                	if(url()->previous()==route('index')){
                		$data['url'] = route('dashboard');
                	}
                	else{
                    	$data['url'] = url()->previous();
                	}
                }
                //return $data;
                //$request->session()->put('api_token',$data['access_token']);
                return view('core.authenticate')->withData($data);
            }
        }
        catch(\Exception $e){
            if(env('APP_DEBUG')){
                return response()->json(['error'=>$e->getMessage()]);
            }
            else{
                return redirect()->route('unauthorized');
            }
        }
        
    }


    public function dashboard(){
        return view('core.dashboard');
    }

    public function invoice(){
        return view('core.invoice');
    }

    public function invoiceCreate(){
        return view('core.invoice_create');
    }

    public function invoiceEdit(){
        return view('core.invoice_edit');
    }

    public function receipt(){
        return view('core.receipt');
    }

    public function receiptCreate(){
        return view('core.receipt_create');
    }

    public function receiptEdit(){
        return view('core.receipt_edit');
    }

    public function creditNote(){
        return view('core.credit_note');
    }

    public function creditNoteCreate(){
        return view('core.credit_note_create');
    }

    public function creditNoteEdit(){
        return view('core.credit_note_edit');
    }

    public function customer(){
        return view('core.customer');
    }

    public function customerEdit(){
        return view('core.customer_edit');
    }

    public function productServices(){
        return view('core.products_services');
    }

    public function productServicesCreate(){
        return view('core.products_services_create');
    }

    public function productServicesEdit(){
        return view('core.products_services_edit');
    }

    public function productServicesTypes(){
        return view('core.products_services_types');
    }

    public function profile(){
        return view('core.dashboard');
    }

    public function taxes(){
        return view('core.taxes');
    }

    
}
