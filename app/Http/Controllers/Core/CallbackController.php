<?php

namespace App\Http\Controllers\Core;



use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Helpers\ValidateToken;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class CallbackController extends Controller{

    public function redirect(Request $request){
        if(env('SSO_MODE')){
            $query = http_build_query([
                'client_id' => env('CLIENT_ID'),
                'redirect_uri' => route('sso.callback'),
                'response_type' => 'code',
                'scope' => '',
                'prompt' => 'none',
            ]);

            $url = env('SSO_URL').'/oauth/authorize?'.$query;
            return redirect($url);
        }
        else{
            return redirect()->route('login');
        }
    }

    public function callback(Request $request){
        if(!env('SSO_MODE')){
            return redirect()->route('login');
        }
        else{
            $error = $request->input('error');
            $code = $request->input('code');

            if($error){
                return response()->json(['msg'=>'Error']);
            }
            else if(!$code){
                return redirect()->route('index');
            }

            if($request->session()->has('access_token')){
                $ValidateToken = new ValidateToken($request->session()->get('access_token'));
                if($ValidateToken->validate()==200){
                    return redirect()->route('index');
                }
            }
            
            try{
                $http = new Client;
                $url = env('SSO_URL').'/oauth/token';
                $response = $http->post($url,[
                    'form_params' => [
                        'grant_type' => 'authorization_code',
                        'client_id' => env('CLIENT_ID'),
                        'client_secret' => env('CLIENT_SECRET'),
                        'redirect_uri' => route('sso.callback'),
                        'code' => $code,
                    ],
                ]);

                $data = json_decode($response->getBody(),true);
                $request->session()->put('access_token',$data['access_token']);

                return redirect()->route('index');
            }
            catch(\Exception $e){
                $token = $request->session()->get('access_token');
                return response()->json(['err'=>$e->getMessage(), 'token'=>$token ]);
            }
        }
    }
    

    public function request(Request $request){
        $http = new Client;

        $url = env('SSO_URL').'/oauth/token';

        $response = $http->post($url,[
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => env('CLIENT_ID'),
                'client_secret' => env('CLIENT_SECRET'),
                'redirect_uri' => route('sso.callback'),
                //'username'=>'noremil@ixtelecom.net',
                //'password'=>'password'
            ],
        ]);

        $data = json_decode($response->getBody(),true);
        $request->session()->put('access_token',$data['access_token']);

        return redirect(route('dashboard'));
    }

}
