<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/redirect','Core\CallbackController@redirect')->name('sso.redirect');
Route::get('/callback','Core\CallbackController@callback')->name('sso.callback');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/unauthorized',function(){ 
	return view('core.unauthorized');
})->name('unauthorized');

Route::group(['middleware' => ['auth.sso'] ,'namespace'=>'Core'],function(){    
    //Route::get('{any}', function () { 
    //    return view('master'); 
    //})->where('any', '.*')->name('index'); 

    Route::get('/','ViewController@index')->name('index');
    Route::get('/dashboard','ViewController@dashboard')->name('dashboard');
    Route::get('/invoice','ViewController@invoice')->name('invoice');
    Route::get('/invoice/create','ViewController@invoiceCreate')->name('invoice.create');
    Route::get('/invoice/{a}','ViewController@invoiceEdit')->name('invoice.edit');

    Route::get('/customer','ViewController@customer')->name('customer');
    Route::get('/customer/{a}','ViewController@customerEdit')->name('customer.edit');
    Route::get('/profile','ViewController@profile')->name('profile');
    
    Route::get('/receipt','ViewController@receipt')->name('receipt');
    Route::get('/receipt/create','ViewController@receiptCreate')->name('receipt.create');
    Route::get('/receipt/{a}','ViewController@receiptEdit')->name('receipt.edit');

    Route::get('/credit_note','ViewController@creditNote')->name('credit_note');
    Route::get('/credit_note/create','ViewController@creditNoteCreate')->name('credit_note.create');
    Route::get('/credit_note/{a}','ViewController@creditNoteEdit')->name('credit_note.edit');

    Route::get('/taxes','ViewController@taxes')->name('taxes');


    Route::get('/products_services','ViewController@productServices')->name('product_services');
    Route::get('/products_services/create','ViewController@productServicesCreate')->name('product_services.create');
    Route::get('/products_services/types','ViewController@productServicesTypes')->name('product_services.types');
    Route::get('/products_services/{a}','ViewController@productServicesEdit')->name('product_services.edit');
});
